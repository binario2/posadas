<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_v1 extends CI_Migration {

        public function up()
        {       
        	$this->db->query("CREATE TABLE IF NOT EXISTS reservas_20211212 SELECT * FROM reservas");
        	$this->db->query("
        		ALTER TABLE reservas ADD COLUMN IF NOT EXISTS embarcacion BOOLEAN NULL DEFAULT FALSE AFTER monto, 
        							 ADD COLUMN IF NOT EXISTS carnadas BOOLEAN NULL DEFAULT '0' AFTER embarcacion, 
        							 ADD COLUMN IF NOT EXISTS guia VARCHAR(255) NULL AFTER carnadas, 
        							 ADD COLUMN IF NOT EXISTS observaciones TEXT NULL AFTER guia
        	");
        }
        public function down()
        {
            $this->db->query("DROP TABLE IF EXISTS reservas_20211212");
            $this->db->query("
        		ALTER TABLE reservas DROP COLUMN IF EXISTS embarcacion, 
        							 DROP COLUMN IF EXISTS carnadas, 
        							 DROP COLUMN IF EXISTS guia, 
        							 DROP COLUMN IF EXISTS
        	");
        }
}