<div id="comprasPorMesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $resumen = $this->db->query("
        SELECT
		sum(t.Enero) as ENE,
		sum(t.Febrero) as FEB,
		sum(t.Marzo) as MAR,
		sum(t.Abril) as ABR,
		sum(t.Mayo) as MAY,
		sum(t.Junio) as JUN,
		sum(t.Julio) as JUL,
		sum(t.Agosto) as AGO,
		sum(t.Setiembre) as SEP,
		sum(t.Octubre) as OCT,
		sum(t.Noviembre) as NOV,
		sum(t.Diciembre) as DIC
		FROM(
		SELECT
		if(month(compras.fecha)=1,cd.total,0) as Enero,
		if(month(compras.fecha)=2,cd.total,0) as Febrero,
		if(month(compras.fecha)=3,cd.total,0) as Marzo,
		if(month(compras.fecha)=4,cd.total,0) as Abril,
		if(month(compras.fecha)=5,cd.total,0) as Mayo,
		if(month(compras.fecha)=6,cd.total,0) as Junio,
		if(month(compras.fecha)=7,cd.total,0) as Julio,
		if(month(compras.fecha)=8,cd.total,0) as Agosto,
		if(month(compras.fecha)=9,cd.total,0) as Setiembre,
		if(month(compras.fecha)=10,cd.total,0) as Octubre,
		if(month(compras.fecha)=11,cd.total,0) as Noviembre,
		if(month(compras.fecha)=12,cd.total,0) as Diciembre
		FROM compras
		INNER JOIN compradetalles cd on cd.compra = compras.id
		WHERE compras.status = 0 and year(compras.fecha)=".$year.") as t
    ")->row();
?>

<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Resumen de compras por mes (<?= $year ?>)</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" data-toggle="tab" href="">Año</a>

                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <a class="dropdown-item" href="javascript:changeYearcomprasPorMes(<?= $i ?>)"><?= $i ?></a>
                            <?php endfor ?>     
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">                        						
                        <canvas id="comprasPorMes" width="auto" height="300"></canvas>
                    </div>
                </div>
            </div>
</div>

<script>
	function loadCharcomprasPorMes(){        
        var chartContainer = KTUtil.getByID('comprasPorMes');
        var chartData = {
            labels: <?php 
                $data = array();
                foreach($resumen as $n=>$v){
                    $data[] = $n;
                }
                echo json_encode($data);
              ?>,
            datasets: [{
                //label: 'Dataset 1',
                backgroundColor: KTApp.getStateColor('success'),
                data: <?php 
                    $data = array();
                    foreach($resumen as $n=>$v){
                        $data[] = $v;
                    }
                    echo json_encode($data);
                  ?>
            }]
        };
        var chart = new Chart(chartContainer, {
            type: 'bar',
            data: chartData,
            options: {
                title: {
                    display: true,
                },
                tooltips: {
                    intersect: true,
                    mode: 'nearest',
                    xPadding: 10,
                    yPadding: 10,
                    caretPadding: 10
                },
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
                barRadius: 4,
                scales: {
                    xAxes: [{
                        display: true,
                        gridLines: true,
                        stacked: true
                    }],
                    yAxes: [{
                        display: true,
                        stacked: true,
                        gridLines: true
                    }]
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }
                }
            }
        });        
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadCharcomprasPorMes();
    <?php else: ?>
        window.afterLoad.push(loadCharcomprasPorMes);        
    <?php endif ?>

    function changeYearcomprasPorMes(y){
        $.post('dashboards/refresh/compras_por_mes',{year:y},function(data){
            $("#comprasPorMesContent").html(data);
        });
    }
</script>
</div>