<div id="grafico_ventas_mesContent">
<?php 
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
        categoriaproducto.denominacion as categoria, 
        TRUNCATE(SUM(ventadetalle.totalcondesc),0) as total 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        INNER JOIN productos on productos.codigo = ventadetalle.producto 
        INNER JOIN clientes on clientes.id = ventas.cliente 
        INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id 
        WHERE ventas.status = 0 and YEAR(ventas.fecha) = '".$year."' group by productos.categoria_id order by sum(ventadetalle.totalcondesc) desc
    ");    
?>
<!--begin:: Widgets/Profit Share-->
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
          <span class="kt-portlet__head-icon kt-hidden">
            <i class="la la-gear"></i>
          </span>
          <h3 class="kt-portlet__head-title">Totales por categoría</h3>
        </div>
        <div class="widget-toolbar">
            <div class="widget-menu">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <i class="ace-icon fa fa-bars"></i>
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" data-toggle="tab">Año</a>

                    <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                    <a class="dropdown-item" href="javascript:changeYeartotalesPorCategoriaGrafico(<?= $i ?>)"><?= $i ?></a>
                    <?php endfor ?>     
                </div>
            </div>
        </div>
    </div>

	<div class="kt-portlet kt-portlet--height-fluid">
		<div class="kt-widget14">
			<div class="kt-widget14__header">
				<h3 class="kt-widget14__title">
					Resumen de ventas por categoria
				</h3>
				<span class="kt-widget14__desc">
					Se muestran las categorías más vendidas
				</span>
			</div>
			<div class="kt-widget14__content">
				<div class="kt-widget14__chart">
					<div class="kt-widget14__stat">
						<?= $year ?>
					</div>
					<canvas id="grafico_ventas_mes" style="height: 280px; width: 280px;"></canvas>
				</div>
				<!--<div class="kt-widget14__legends">
					<?php foreach($qry->result() as $n=>$v): ?>
						<div class="kt-widget14__legend">
							<span class="kt-widget14__bullet kt-bg-success"></span>
							<span class="kt-widget14__stats"><?= number_format($v->total,0,',','.') ?>Gs <?= $v->categoria ?></span>
						</div>	                        
		             <?php endforeach ?>
				</div>-->
			</div>
		</div>
	</div>
</div>

<!--end:: Widgets/Profit Share-->
<script>

    function loadChartotalesPorCategoriaGrafico(){        
        if (!KTUtil.getByID('grafico_ventas_mes')) {
            return;
        }

        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };

        var config = {
            type: 'doughnut',
            data: {
            	labels: <?php 
	                $data = array();
	                foreach($qry->result() as $n=>$v){
	                    $data[] = $v->categoria;
	                }
	                echo json_encode($data);
	              ?>,
	            datasets: [{
	                //label: 'Dataset 1',
	                backgroundColor: [
                        KTApp.getStateColor('success'),
                        KTApp.getStateColor('info'),
                        KTApp.getStateColor('primary'),
                        KTApp.getStateColor('warning'),
                        KTApp.getStateColor('danger'),
                        KTApp.getStateColor('brand'),
                        KTApp.getStateColor('dark'),
                        KTApp.getStateColor('brand10'),
                        KTApp.getStateColor('brand2'),
                        KTApp.getStateColor('brand3'),
                        KTApp.getStateColor('brand4'),
                        KTApp.getStateColor('brand5'),
                        KTApp.getStateColor('brand6'),
                        KTApp.getStateColor('brand7'),
                        KTApp.getStateColor('brand8'),
                        KTApp.getStateColor('brand9'),
                    ],
	                data: <?php 
	                    $data = array();
	                    foreach($qry->result() as $n=>$v){
	                        $data[] = $v->total;
	                    }
	                    echo json_encode($data);
	                  ?>
	            }]
            },
            options: {
                cutoutPercentage: 75,
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    display: false,
                    position: 'top',
                },
                title: {
                    display: false,
                    text: 'Technology'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                tooltips: {
                    enabled: true,
                    intersect: true,
                    mode: 'nearest',
                    bodySpacing: 5,
                    yPadding: 10,
                    xPadding: 10, 
                    caretPadding: 0,
                    displayColors: true,
                    backgroundColor: KTApp.getStateColor('brand'),
                    titleFontColor: '#ffffff', 
                    cornerRadius: 4,
                    footerSpacing: 0,
                    titleSpacing: 0
                }
            }
        };

        var ctx = KTUtil.getByID('grafico_ventas_mes').getContext('2d');
        var myDoughnut = new Chart(ctx, config);  
    }
    

    <?php if(!empty($_POST['year'])): ?>
        loadChartotalesPorCategoriaGrafico();
    <?php else: ?>
        window.afterLoad.push(loadChartotalesPorCategoriaGrafico);        
    <?php endif ?>

    function changeYeartotalesPorCategoriaGrafico(y){
        $.post('dashboards/refresh/grafico_ventas_mes',{year:y},function(data){
            $("#grafico_ventas_mesContent").html(data);
        });
    }
</script>
</div>