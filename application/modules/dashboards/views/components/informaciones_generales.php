<?php 
    $cobpendientes = $this->db->query("
        SELECT 
        format(SUM(total.saldo),0,'de_DE') AS t_saldo
        FROM 
        (SELECT 
        clientes.id,
        round(ifnull(compra.Total_compra,0),0) as t_compra,
        ifnull(pagos.total_pago,0) as t_pago,
        ifnull(notas.total_credito,0) as t_credito,
        (ifnull(pagos.total_pago,0)+ifnull(notas.total_credito,0)) as t_pagado,
        IF(ifnull(compra.Total_compra,0)>(ifnull(pagos.total_pago,0)+ifnull(notas.total_credito,0)), 
        round((ifnull(compra.Total_compra,0)-(ifnull(pagos.total_pago,0)+ifnull(notas.total_credito,0))),0), 0) as saldo 
        FROM clientes  
        INNER JOIN(
        SELECT 
        ventas.cliente as cliente_id, 
        sum(ventadetalle.totalcondesc) as Total_compra, 
        MAX(date(ventas.fecha)) as fecha_ult_compra 
        FROM ventas 
        INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
        WHERE ventas.status = 0 and ventas.transaccion = 2 AND ventas.cliente != 1 
        GROUP BY ventas.cliente) compra on compra.cliente_id=clientes.id 
        LEFT JOIN (
        SELECT 
        pagocliente.clientes_id as cliente_id,
        sum(pagocliente.total_pagado) as total_pago, 
        max(date(pagocliente.fecha)) as fecha_ult_pago 
        FROM pagocliente 
        WHERE pagocliente.anulado = 0 
        GROUP by pagocliente.clientes_id) pagos on pagos.cliente_id=clientes.id 
        LEFT JOIN (
        SELECT 
        notas_credito_cliente.cliente as cliente_id,
        SUM(notas_credito_cliente.total_monto) as total_credito,
        MAX(date(notas_credito_cliente.fecha)) as fecha_ult_nota 
        FROM notas_credito_cliente where notas_credito_cliente.anulado = 0 
        GROUP BY notas_credito_cliente.cliente) notas on notas.cliente_id=clientes.id) total
    ")->row()->t_saldo;
?>  

<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Informaciones generales (en cantidades)</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                    </div>

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">
                        <div class="row" style="margin-left: 0; margin-right: 0">
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Efectivo a rendir</span>
                                      <?php $qry = $this->db->query("
                                                SELECT 
                                                format((sum(efectivo.Ingreso)+sum(efectivo.Caja_inicial))-sum(efectivo.Egreso),0,'de_DE') as total 
                                                FROM(
                                                SELECT
                                                if(consulta.Tipo = 'Caja_inicial',consulta.total,0) as Caja_inicial, 
                                                if(consulta.Tipo = 'Ingreso',consulta.total,0) as Ingreso, 
                                                if(consulta.Tipo = 'Egreso',consulta.total,0) as Egreso
                                                FROM( 
                                                SELECT 
                                                'Caja_inicial' as Tipo, 
                                                'Monto_inicial' as Movimiento, 
                                                cajadiaria.monto_inicial as total 
                                                FROM 
                                                cajadiaria WHERE cajadiaria.id = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT
                                                'Ingreso' as Tipo,
                                                'Venta_contado' as Movimiento,
                                                ifnull(sum(ventadetalle.totalcondesc),0) as total
                                                FROM ventas
                                                INNER JOIN ventadetalle on ventadetalle.venta = ventas.id
                                                INNER JOIN productos on productos.codigo = ventadetalle.producto
                                                WHERE ventas.transaccion = 1 and ventas.status = 0 and ventas.cajadiaria = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT 
                                                'Ingreso' as Tipo, 
                                                'Entrega_credito' as Movimiento, 
                                                ifnull(sum(cr.entrega_inicial),0) as total 
                                                FROM creditos cr 
                                                INNER JOIN ventas on ventas.id = cr.ventas_id WHERE cr.anulado = 0 or cr.anulado is null and ventas.status != -1 and ventas.cajadiaria = 3
                                                UNION ALL 
                                                SELECT
                                                'Ingreso' as Tipo,
                                                'Pago_credito' as Movimiento, 
                                                ifnull(sum(total_pagado),0) as total 
                                                FROM pagocliente WHERE (anulado = 0 or anulado is null) and cajadiaria = ".$this->user->cajadiaria."
                                                UNION ALL 
                                                SELECT 
                                                'Egreso' as Tipo, 
                                                'Gastos_varios' as Movimiento, 
                                                IFNULL(sum(gastos.monto),0) as total
                                                FROM gastos WHERE 1 and gastos.cajadiaria = ".$this->user->cajadiaria.") AS consulta) AS efectivo"); ?>
    							      <div class="caption">
    							        <h4 title="<?= $qry->num_rows()>0?$qry->row()->total:0 ?>"><?php 
                                                echo cortarNumero(str_replace('.','',$qry->num_rows()>0?$qry->row()->total:0));
                                        ?></h4>               
    							      </div>
    							    </div>
    							</a>
    						</div>

    						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="text-align: center">
    						  	<a href="#">
    							    <div class="thumbnail">
    							      <span>Clientes</span>
    							      <div class="caption">
    							        <h4 title="<?= $this->db->query("SELECT COUNT(id) total FROM `clientes` WHERE id >1")->row()->total ?>"><?php 
                                            echo cortarNumero(str_replace('.','',$this->db->query("SELECT COUNT(id) total FROM `clientes` WHERE id >1")->row()->total));
                                        ?></h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
    						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="text-align: center">
    						  	<a href="#" title="Cobros pendientes">
    							    <div class="thumbnail">
    							      <span>Cob. pendientes</span>
    							      <div class="caption">
    							        <h4 title="<?= $cobpendientes ?>"><?= cortarNumero(str_replace('.','',$cobpendientes)) ?></h4>       
    							      </div>
    							    </div>
    							</a>
    						</div>
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" style="text-align: center">
                                <a href="#" title="Inventario valorizado">
                                    <div class="thumbnail">
                                      <span>Inv. valorizado</span>
                                      <?php $inv = $this->db->query("SELECT 
                                                format(round(sum(inv.stock * productos.precio_costo),0),0,'de_DE') as total
                                                FROM productosucursal inv
                                                INNER JOIN productos on productos.codigo = inv.producto
                                                WHERE productos.inventariable = 1")->row()->total; ?>
                                      <div class="caption">
                                        <h4 title="<?= $inv ?>"><?= cortarNumero(str_replace('.','',$inv)) ?></h4>       
                                      </div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
</div>
<script>

</script>
