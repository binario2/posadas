<div id="top10ProductosMasVendidosContent">
<?php
    $year = empty($_POST['year'])?date("Y"):$_POST['year'];
    $qry = $this->db->query("
        SELECT 
		productos.nombre_comercial as Producto,
		format(sum(ventadetalle.cantidad),0,'de_DE') as Cantidad,
		format(sum(ventadetalle.totalcondesc),0,'de_DE') as Total
		FROM ventas 
		INNER JOIN ventadetalle on ventas.id = ventadetalle.venta 
		INNER JOIN productos on productos.codigo = ventadetalle.producto 
		INNER JOIN clientes on clientes.id = ventas.cliente 
		INNER JOIN categoriaproducto on categoriaproducto.id = productos.categoria_id 
		WHERE ventas.status = 0 AND YEAR(ventas.fecha) = '".$year."'
		group by productos.id  
		ORDER BY sum(ventadetalle.totalcondesc)  DESC
		LIMIT 10
    ");
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title">Top 10 productos más vendidos (<?= $year ?>)</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-bars"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right dropdown-light-blue dropdown-caret dropdown-closer">
                            <li>
                                <a href="#dropdown1" data-toggle="tab"><b>Año</b></a>
                            </li>              
                            <?php for($i = date("Y")-3;$i<date("Y")+3;$i++): ?>
                            <li>
                                <a href="javascript:changeYeartop10ProductosMasVendidos(<?= $i ?>)"><?= $i ?></a>
                            </li>
                            <?php endfor ?>              
                        </ul>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main no-padding">
                        <?php sqlToHtml($qry); ?>

                    </div>
                </div>
            </div>
</div>
<script>    
    function changeYeartop10ProductosMasVendidos(y){
        $.post('dashboards/refresh/top_10_productos_mas_vendidos_admin',{year:y},function(data){
            $("#top10ProductosMasVendidosContent").html(data);
        });
    }
</script>
</div>