<div id="transferenciasContent">
<?php 
	$transferencias = $this->db->query(
		"
			SELECT 
			transferencias.id,
			so.denominacion AS sucursal_origen,
			sd.denominacion AS sucursal_destino,
			so.id AS sucursal_origen_id,
			sd.id AS sucursal_destino_id,
			fecha_solicitud,
			procesado,
			user_id
			FROM transferencias
			INNER JOIN sucursales so ON so.id = sucursal_origen
			INNER JOIN sucursales sd ON sd.id = sucursal_destino
			WHERE 
				procesado >= 0 AND procesado < 2 
				AND (
					(sucursal_origen = '{$this->user->sucursal}' AND procesado = 0) OR 
					(sucursal_destino = '{$this->user->sucursal}' AND procesado = 1)
				)

			ORDER BY transferencias.id DESC
		"
	);
?>
<div class="kt-portlet transparent ui-sortable-handle" data-id="4">
   
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                  <span class="kt-portlet__head-icon kt-hidden">
                    <i class="la la-gear"></i>
                  </span>
                  <h3 class="kt-portlet__head-title"> Resumen de transferencias pendientes</h3>
                </div>

                <div class="widget-toolbar">
                    <div class="widget-menu">
                        <a data-toggle="dropdown" data-action="settings" href="#">
                            <i class="ace-icon fa fa-list"></i>
                        </a>
                    </div>

                    <!--<a class="orange2" data-action="fullscreen" href="#">
                        <i class="ace-icon fa fa-expand"></i>
                    </a>

                    <a data-action="reload" href="#">
                        <i class="ace-icon fa fa-refresh"></i>
                    </a>-->

                    <a data-action="collapse" href="#">
                        <i class="ace-icon fa fa-chevron-up"></i>
                    </a>

                    <!--<a data-action="close" href="#">
                        <i class="ace-icon fa fa-times"></i>
                    </a>-->
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="kt-section">
                    <div class="widget-main">                        
						<?php 
							sqlToHtml($transferencias,array('id','sucursal_origen','sucursal_destino','fecha_solicitud','procesado'),array(),array(
								'procesado'=>function($val,$row){
									if ($row->sucursal_origen_id == $this->user->sucursal && $val == 0){
					                    return form_dropdown('procesado', array('0' => 'Sin procesar', '-1' => 'Rechazado', '1' => 'Aprobado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
					                }
					                elseif ($row->sucursal_destino_id == $this->user->sucursal && $val == 1){
					                    return form_dropdown('procesado', array('1' => 'Aprobado', '2' => 'Entregado'), $val, 'class="procesado form-control" data-rel="' . $row->id . '"');
					                }
					                return $val;
								}
							));
						?>
                    </div>
                </div>
            </div>
</div>

<script>
	window.afterLoad.push(function(){
		$(document).on('change','.procesado',function(){
			$.post('<?= base_url('json/transferencia_status_change') ?>',{id:$(this).data('rel'),val:$(this).val()},function(data){
	            emergente(data);
	            $.post('dashboards/refresh/transferencias',{},function(data){
		            $("#transferenciasContent").html(data);
		        });
	        });
		});
	});
</script>
</div>