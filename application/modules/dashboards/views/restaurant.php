<?php 
  $iconos = [
    'mesas'=>'utensils',
    'habitaciones'=>'bed',
    'parrillas'=>'drumstick-bite',
    'campings'=>'campground',
    'deliverys'=>'motorcycle',
    'barras'=>'chair'
  ];
  $estados = [
      0=>'default',
      1=>'primary',
      2=>'warning',
      3=>'secondary'
  ]; 
?>
<div class="accordion" id="accordionExample">
  
  <?php 
    $this->db->order_by('orden','ASC');
    foreach($this->db->get('orden_lugares')->result() as $n=>$servicio): 
  ?>
    <div class="card">
      <div class="card-header" id="heading<?= $n ?>">
        <h2 class="mb-0">
          <button class="btn btn-link btn-block text-left <?= $n==0?'':'collapsed' ?>" type="button" data-toggle="collapse" data-target="#<?= $servicio->servicio ?>" aria-expanded="false" aria-controls="collapseFour">
            <i class="fa fa-<?= $iconos[$servicio->servicio] ?>"></i> <?= ucfirst($servicio->servicio) ?>
          </button>
        </h2>
      </div>
      <div id="<?= $servicio->servicio ?>" class="<?= $n==0?'collapse show':'collapse' ?>" aria-labelledby="headingFour" data-parent="#accordionExample">
        <div class="card-body">
          <?php $this->load->view('components/restaurant/'.$servicio->servicio,['estados'=>$estados]) ?>
        </div>
      </div>
    </div>
  <?php endforeach ?>
  


</div>
<script>
    function reservarHabitacion(id, tipo) {
        var data = {
            sucursales_id:<?= $this->user->sucursal ?>,
            facturado: 0,
            fecha_pedido: '<?= date("Y-m-d H:i:s") ?>',
            mozos_id: '<?= !empty($this->user->mozo) ? $this->user->mozo : 0 ?>',
            user_id:<?= $this->user->id ?>
        };

        switch (tipo) {
            case 'habitacion':
                data.habitaciones_id = id;
                data.tipo_pedidos_id = 1;
                break;
            case 'parrilla':
                data.parrillas_id = id;
                data.tipo_pedidos_id = 2;
                break;
            case 'camping':
                data.campings_id = id;
                data.tipo_pedidos_id = 3;
                break;
            case 'mesa':
                data.mesas_id = id;
                data.tipo_pedidos_id = 4;
                break;
            case 'delivery':
                data.deliverys_id = id;
                data.tipo_pedidos_id = 5;
                break;
            case 'barra':
                data.barras_id = id;
                data.tipo_pedidos_id = 6;
                break;
        }
        data.cajadiaria = '<?= $_SESSION['cajadiaria'] ?>';
        $.post("<?= base_url("pedidos/admin/pedidos/insert") ?>", data, function (data) {
            data = data.replace('<textarea>', '', data);
            data = data.replace('</textarea>', '', data);
            data = JSON.parse(data);
            if (data.success) {
                document.location.href = "<?= base_url('pedidos/admin/pedidos_detalles') ?>/" + data.insert_primary_key + '/';
            } else {
                alert("Ha ocurrido un error al reservar la habitación");
                document.location.reload();
            }
        });
    }
</script>