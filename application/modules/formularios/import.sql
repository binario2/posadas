CREATE TABLE `formularios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `unset_add` tinyint(1) DEFAULT 0,
  `unset_edit` tinyint(1) DEFAULT 0,
  `unset_export` tinyint(1) DEFAULT 0,
  `unset_print` tinyint(1) DEFAULT 0,
  `unset_delete` tinyint(1) DEFAULT 0,
  `unset_list` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `formularios_campos` (
  `id` int(11) NOT NULL,
  `formularios_id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `etiqueta` varchar(255) DEFAULT NULL,
  `valores` text DEFAULT NULL,
  `tipo` varchar(255) DEFAULT 'text',
  `requerido` tinyint(1) DEFAULT 0,
  `mostrar_listado` tinyint(1) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `formularios`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `formularios_campos`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `formularios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `formularios_campos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;