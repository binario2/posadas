<?php

require_once APPPATH.'/controllers/Panel.php';    

class PedidosProveedores extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }
    
    public function pedidos_proveedores($x = '',$y = '',$z = '') {
        $crud = parent::crud_function($x, $y);
        $crud->callback_column('status', function($val, $row) {
            switch ($val) {
                case '1':return 'Pagada <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '0':return 'Activa <a title="anular" href="javascript:anular(' . $row->id . ')"><i class="glyphicon glyphicon-remove"></i></a>';
                    break;
                case '-1':return 'Anulada';
                    break;
                case '-2': return 'Anulada y procesada';
                    break;
            }
        });
        $crud->set_rules('total_compra','Total de compra','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto');                        
        $crud->callback_after_insert(array($this,'addBody'));
        $crud->callback_before_update(function($post){            
            echo '<textarea>{"success":false,"message":"La edición no está permitida"}</textarea>';
            die();
        });
        $crud->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->order_by('pedidos_proveedores.id','DESC');
        $crud->columns('sucursal','fecha','tipo_facturacion_id','proveedor','total_compra','status');
        $crud->add_action('Facturar','',base_url('movimientos/compras/compras/add/PedidosProveedores/').'/');
        $output = $crud->render();
        $output->crud = 'user';
        if ($crud->getParameters()!='list'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('pedidos_proveedores',array('output'=>$output->output,'edit'=>$edit),TRUE);            
        }
        $this->loadView($output);
    }

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        //$this->db->delete('ventadetalle',array('venta'=>$primary));
        $total = 0;       
        foreach($productos as $p){
            $pro[] = $p->nombre_comercial; 
            $this->db->insert('pedidos_proveedores_detalles',array(
                'pedidos_proveedores'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,
                'lote'=>'',
                'vencimiento'=>'0000-00-00',
                'precio_costo'=>$p->precio_costo,
                'por_desc'=>$p->por_desc,
                'por_venta'=>$p->por_venta,
                'precio_venta'=>$p->precio_venta,
                'total'=>$p->total
            ));
            $total+= $p->total;
        }
        $this->db->update('pedidos_proveedores',array('productos'=>implode($pro),'total_compra'=>$total),array('id'=>$primary));
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
