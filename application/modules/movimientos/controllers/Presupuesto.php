<?php

require_once APPPATH.'/controllers/Panel.php';    

class Presupuesto extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }
    }
    
    public function presupuesto($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read();

        $crud->columns('id','transaccion', 'cliente', 'fecha', 'caja', 'total_presupuesto','usuario');
        
        if($this->user->admin!=1){
            $crud->unset_edit()
                 ->unset_delete();
        }
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
             ->set_relation('cliente', 'clientes', '{nombres} {apellidos}')
             ->set_relation('sucursal', 'sucursales', 'denominacion')
             ->set_relation('usuario','user','{nombre} {apellido}')
             ->display_as('usuario','Vendedor');
        $crud->where('presupuesto.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('presupuesto.caja', $_SESSION['caja']);
        }        
        $crud->order_by('id','DESC');        
        ///Validations
        $crud->set_rules('total_presupuesto','Total de presupuesto','required|numeric|greater_than[0]');        
        $crud->set_rules('productos','Productos','required|callback_unProducto|callback_validar');                        
        $crud->callback_before_insert(array($this,'validarFacturacion'));
        $crud->callback_after_insert(array($this,'addBody'));        
        $crud->callback_after_update(array($this,'addBody'));        
        $crud->callback_after_delete(function($primary){get_instance()->db->delete('presupuesto_detalle',array('presupuesto_id'=>$primary));});        
        $crud->unset_back_to_list();
        $crud->add_action('Facturar','',base_url('movimientos/ventas/ventas/add/Presupuesto/').'/');
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = '';
            if($crud->getParameters()=='edit'){
                $edit = $y;                
            }
            $output->output = $this->load->view('presupuesto',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }else{
            $output->output = $this->load->view('presupuesto_list',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);     
    }

    public function presupuestodetalle($x = '', $y = '') {         
        $crud = parent::crud_function($x, $y);
        if(!empty($_POST['presupuesto_id'])){
            $crud->where('presupuesto_id',$_POST['presupuesto_id']);
        }
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$val))->row();
        });        
        $crud->unset_add()->unset_edit()->unset_delete();
        $output = $crud->render();        
        $this->loadView($output);     
    }
    //Callback para validar si la venta almacena el id de factura o no
    function validarFacturacion($post){        
        $post['fecha'] = date("Y-m-d H:i:s");
        return $post;
    }
    //Callback de function ventas a,b insert
    function addBody($post,$primary){
        $productos = json_decode($_POST['productos']);
        $pro = array();
        $this->db->delete('presupuesto_detalle',array('presupuesto_id'=>$primary));
        foreach($productos as $p){
            $pro[] = $p->nombre;
            $p->preciocosto = @$this->db->get_where('productos',array('codigo'=>$p->codigo))->row()->precio_costo;
            $this->db->insert('presupuesto_detalle',array(
                'presupuesto_id'=>$primary,
                'producto'=>$p->codigo,                
                'cantidad'=>$p->cantidad,                
                'precio_venta'=>$p->precio_venta,                
                'total'=>$p->total,
                'iva'=>0
            ));
        }
        $this->db->update('presupuesto',array('productos'=>implode($pro)),array('id'=>$primary));       
    }    

    function unProducto(){
        $productos = $_POST['productos'];     
        $productos = count(json_decode($productos));
        if(empty($productos)){            
            $this->form_validation->set_message('unProducto','Debe incluir al menos un producto');        
            return false;
        }
        //Validar si el pago es completo
    }

    function validar(){ 
        $productos = $_POST['productos'];       
        $ajustes = $this->ajustes;        
        if($_POST['transaccion']==2 && $_POST['cliente']==1){
            $this->form_validation->set_message('validar','Debe indicar a que cliente desea asignarle esta venta a crédito');        
            return false;
        }
        return true;
    }

    function presupuesto_detail($ventaid){
        $this->as['ventas_detail'] = 'presupuesto_detalle';
        $crud = $this->crud_function('','');
        $crud->where('presupuesto_id',$ventaid);
        $crud->columns('producto','cantidad','totalcondesc');
        $crud->callback_column('producto',function($val,$row){
            return $this->db->get_where('productos',array('codigo'=>$row->producto))->roW()->nombre_comercial;
        }); 
        $crud->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_export()
             ->unset_print()
             ->unset_read();
        $crud->staticShowList = true;
        $crud = $crud->render();
        echo $crud->output;

    }
    /* Cruds */    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
