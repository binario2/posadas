<?php

require_once APPPATH.'/controllers/Panel.php';    

class Productos extends Panel {

    function __construct() {
        parent::__construct();        
    }     
    
    public function categorias($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','denominacion','control_vencimiento');
        if($crud->getParameters()=='add'){
            $crud->set_rules('denominacion','Denominación','required|is_unique[categoriaproducto.denominacion]');
        }
        $crud->unset_delete();
        $crud->field_type('control_vencimiento', 'true_false', array(0 => 'No', 1 => 'Si'));
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_categoria_producto($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_categoria_producto','categoriaproducto_id');
        $crud->display_as('nombre_sub_categoria_producto','nombre')
             ->display_as('categoriaproducto_id','categoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }

    public function sub_sub_categoria($x = '', $y = '') {
        $crud = parent::crud_function($x, $y);
        $crud->columns('id','nombre_sub_sub_categoria','sub_categoria_producto_id');
        $crud->display_as('nombre_sub_sub_categoria','nombre')
             ->display_as('sub_categoria_producto_id','SubCategoria');
        $crud->unset_delete();        
        $output = $crud->render();
        $this->loadView($output);
    }
    
    public function productos($x = '', $y = '', $return = false) {
        if($x=='codigoInterno'){
            echo $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
            return;
        }
        if($x=='masDetalles' && !empty($y)){
            $this->load->view('_productosMasDetallesModal',['producto'=>$y]);
            return;
        }
        $crud = parent::crud_function($x, $y, $this);
        $crud->set_theme('bootstrap');
        if(in_array('Cajeros',$this->user->getGroupsArray('nombre'))){            
            $crud->columns('id','codigo', 'codigo_interno','nombre_comercial', 'nombre_generico', 'propiedades','proveedor_id', 'categoria_id','precio_venta','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','descmax','stock','anulado');
            $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read();
        }else{
            $crud->columns('codigo', 'codigo_interno','nombre_comercial', 'nombre_generico', 'propiedades', 'categoria_id', 'precio_costo','precio_venta','precio_venta_mayorista1','precio_venta_mayorista2','precio_venta_mayorista3','stock');
        }
        $crud->set_relation('proveedor_id', 'proveedores', 'denominacion');
        $crud->set_relation('categoria_id', 'categoriaproducto', 'denominacion');
        $crud->set_relation('nacionalidad_id', 'paises', 'denominacion');
        $crud->callback_column('stock',function($val,$row){
            $sucursal = empty($_POST['sucursal'])?$this->user->sucursal:$_POST['sucursal'];
            return @$this->db->get_where('productosucursal',array('producto'=>strip_tags($row->codigo),'sucursal'=>$sucursal))->row()->stock;
        });
        if($x!='json_list'){
            $crud->callback_column('codigo',function($val,$row){            
                return '<a href="javascript:;" onclick="masDetalles(\''.$val.'\')">'.$val.'</a>';
            });
        }
        if (empty($x) || $x == 'ajax_list' || $x == 'success'){
            $crud->set_relation('usuario_id', 'user', 'nombre');        
        }
        //Etiquetas
        $crud->display_as('proveedor_id', 'Proveedor')
                ->display_as('categoria_id', 'Categoria')
                ->display_as('id', 'ID de producto')
                ->display_as('nacionalidad_id', 'Nacionalidad')
                ->display_as('descmin', 'Minimo de descuento')
                ->display_as('descmax', 'Maximo de descuento')
                ->display_as('usuario_id', 'Usuario')
                ->field_type('iva_id', 'dropdown', array('0' => 'Exento', '5' => '5%', '10' => '10%'))
                ->field_type('no_inventariable', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('no_caja', 'true_false', array('0' => 'NO', '1' => 'SI'))
                ->field_type('foto_principal','image',array('path'=>'img/productos','width'=>'150px','height'=>'150px'))
                ->display_as('iva_id', 'IVA')
                ->display_as('no_inventariable','Inventariable');
        
        //Validaciones
        if($crud->getParameters()=='add'){
            $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
        }        
        if($crud->getParameters()=='edit' && isset($_POST['codigo']) && isset($_POST['codigo_interno'])){
            $producto = $this->db->get_where('productos',['id'=>$y])->row();
            if($producto->codigo!=$_POST['codigo']){
                $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo]');
            }
            if($producto->codigo_interno!=$_POST['codigo_interno']){
                $crud->set_rules('codigo', 'Codigo', 'required|is_unique[productos.codigo_interno]');
            }
        }
        $crud->field_type('usuario_id', 'hidden', $_SESSION['user']);
        if (!empty($x) && !empty($y) && $y == 'json' && !empty($return)) {
            $crud->field_type('codigo', 'hidden', $return);
            $crud->unset_back_to_list();
            $crud->set_lang_string('insert_success_message', '<script>window.opener.tryagain(); window.close();</script>');
        }
        $crud->unset_delete(); 
        $crud->callback_before_insert(function($post){
            $post['codigo_interno'] = $this->db->query('select getUltimoCodigoInterno()+1 as cod')->row()->cod;
            return $post;
        });
        $crud->callback_before_update(function($post,$primary){
            $producto = get_instance()->db->get_where('productos',array('id'=>$primary));
            if($producto->row()->precio_venta!=$post['precio_venta']){
                get_instance()->db->update('productosucursal',array('precio_venta'=>$post['precio_venta']),array('producto'=>$producto->row()->codigo));
            }
        })
        ->callback_after_insert(function($post){
            $sucursal = get_instance()->db->get_where('sucursales');
            foreach($sucursal->result() as $s){
                $this->db->insert('productosucursal',[
                    'proveedor_id'=>$post['proveedor_id'],
                    'fechaalta'=>date("Y-m-d H:i:s"),
                    'producto'=>$post['codigo'],
                    'sucursal'=>$s->id,
                    'lote'=>'',
                    'vencimiento'=>'',
                    'precio_venta'=>$post['precio_venta'],
                    'precio_costo'=>$post['precio_costo'],
                    'stock'=>0
                ]);
            }        
            return $post;
        });

        $crud->unset_read();

        $crud->add_action('Productos asociados','',base_url('movimientos/productos/producto_asociado').'/');        
        //Buscador de ventas
        if(!empty($_POST['codigo'])){
            $crud->where("(productos.codigo = '".$_POST['codigo']."' OR productos.codigo_interno = '".$_POST['codigo']."') AND (productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
        }
        $output = $crud->render();   
        if($crud->getParameters()=='add' || $crud->getParameters()=='edit'){
            $edit = (is_numeric($y))?$edit = $this->db->get_where('productos',array('id'=>$y)):'';
            $output->output = $this->load->view('productos',array('output'=>$output->output,'edit'=>$edit),TRUE);
        }
        if($crud->getParameters()=='list'){
            $output->output = $this->load->view('productosList',array('output'=>$output->output),TRUE);
        }
        $this->loadView($output);
    }

    function producto_asociado($x = ''){
        if(is_numeric($x)){
            $crud = parent::crud_function("",'');
            $crud->where('productos_id',$x)
                 ->field_type('productos_id','hidden',$x)
                 ->unset_columns('productos_id')
                 ->set_relation('descontar','productos','{codigo} {codigo_interno} {nombre_comercial}');
            $output = $crud->render();

            $head = new ajax_grocery_crud();
            $head->set_table('productos')->set_theme('header_data')->set_subject('producto')->columns('codigo','nombre_comercial','nombre_generico','propiedades')->where('productos.id',$x)->unset_add()->unset_edit()->unset_delete()->unset_print()->unset_export()->unset_read();
            $output->header = $head->render(1)->output;
            $this->loadView($output);
        }
    }
    
    function buscador_productos(){
        $this->as['buscador_productos'] = 'productos';
        $crud = parent::crud_function("",''); 
        $crud->columns('id','codigo','nombre_comercial','precio_venta','precio_costo','stock');
        if($crud->getParameters(FALSE)!=='json_list'){
            $crud->callback_column('nombre_comercial',function($val,$row){
                return '<a href="javascript:parent.seleccionarProducto('.$row->id.',\''.$val.'\','.$row->precio_venta.','.$row->precio_costo.')">'.$val.'</a>';
            });
        }
        $crud->unset_add()
                 ->unset_edit()
                 ->unset_delete()
                 ->unset_export()
                 ->unset_print()
                 ->unset_read();
        $output = $crud->render();
        $output->view = "json";
        $output->output = $this->load->view('busqueda',array('output'=>$output->output),TRUE);
        $this->loadView($output);
    }
    
    public function inventario($x = '', $y = '') {   
        if($x=='reload'){
            $this->db->query('call set_stock('.$y.')');
            redirect('movimientos/productos/inventario/success');
        }     
        $this->as['inventario'] = 'productosucursal';        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap');
        $crud->unset_read()
             ->unset_export()
             ->unset_print()
             ->unset_edit()
             ->unset_delete()
             ->unset_add();     
        $crud->columns('j286e18ee.codigo','j286e18ee.nombre_comercial','j286e18ee.nombre_generico','stock','precio_venta','sucursal');
        $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}');
        $crud->set_relation('sucursal','sucursales','denominacion');
        $crud->callback_column('scb5fc3d6', function($val, $row) {
            return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
        });
        $crud->callback_column('j286e18ee.codigo', function($val, $row) {
            return explode('|',$row->s286e18ee)[0];
        });
        $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
            return explode('|',$row->s286e18ee)[1];
        });
        $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
            return explode('|',$row->s286e18ee)[2];
        });
        $crud->callback_column('stock', function($val, $row) {
            return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="la la-refresh"></i></a>';
        });
        $crud->display_as('j286e18ee.codigo','Codigo')
             ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
             ->display_as('j286e18ee.nombre_generico','Nombre Genérico');
        $crud->set_primary_key('codigo','productos');
        if (is_numeric($x)) {
            $crud->where('sucursal', $x);
        }else{
            $crud->where('sucursal', $this->user->sucursal);
        }
        $output = $crud->render();
        $output->crud = 'productosucursal';
        $this->loadView($output);
    }

    public function inventario_modal($sucursal = ''){
        if($sucursal == 'ajax_list_info'){
            //echo '{"total_results":300}';
            //return;
        }
        $crud = new ajax_grocery_crud();
        $crud->set_table('productosucursal')
             ->set_subject('Inventario')
             ->set_theme('bootstrap')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()   
             ->field_type('productos_id','hidden')          
             ->callback_column('Codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('j286e18ee.codigo','j286e18ee.codigo_interno','j286e18ee.nombre_comercial','stock','j286e18ee.precio_venta','j286e18ee.precio_venta_mayorista1','j286e18ee.precio_venta_mayorista2');
            $crud->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}|{precio_venta}|{codigo_interno}|{foto_principal}|{precio_costo}|{precio_venta_mayorista1}|{precio_venta_mayorista2}');
            //$crud->set_relation('sucursal','sucursales','denominacion');
            $crud->callback_column('scb5fc3d6', function($val, $row) {
                return '<a href="' . base_url('movimientos/productos/inventario/' . $row->sucursal) . '">' . $val . '</a>';
            });
            $crud->callback_column('j286e18ee.codigo', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[0];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });            
            $crud->callback_column('j286e18ee.nombre_comercial', function($val, $row) {
                $cod = explode('|',$row->s286e18ee)[0];
                $val = explode('|',$row->s286e18ee)[1];
                $img = explode('|',$row->s286e18ee)[5];
                $img = empty($img)?base_url('200x200.png'):base_url('img/productos/'.$img);
                return '<a href="javascript:selCod(\''.$cod.'\',\'\',\'\')">'.$val.'</a> <a href="javascript:showImage(\''.$img.'\')"><i class="fa fa-image"></i></a>';
            });            
            $crud->callback_column('j286e18ee.nombre_generico', function($val, $row) {
                return explode('|',$row->s286e18ee)[2];
            });
            $crud->callback_column('j286e18ee.precio_venta', function($val, $row) {                
                $val = explode('|',$row->s286e18ee)[3];
                return number_format((float)$val,0,',','.');
            });
            $crud->callback_column('j286e18ee.codigo_interno', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[4];
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            });
            $crud->callback_column('j286e18ee.precio_costo', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[6];
                return $val;
            });
            $crud->callback_column('j286e18ee.precio_venta_mayorista1', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[7];
                return $val;
            });
            $crud->callback_column('j286e18ee.precio_venta_mayorista2', function($val, $row) {
                $val = explode('|',$row->s286e18ee)[8];
                return $val;
            });
            $crud->callback_column('stock', function($val, $row) {
                return $val.' <a href="' . base_url('movimientos/productos/inventario/reload/'.$row->producto).'"><i class="fa  fa-refresh "></i></a>';
            });
            $crud->display_as('j286e18ee.codigo','Codigo')
                 ->display_as('j286e18ee.nombre_comercial','Nombre Comercial')
                 ->display_as('j286e18ee.nombre_generico','Nombre Genérico');
            $crud->set_primary_key('codigo','productos');

        if(!empty($this->user->sucursal) && !is_numeric($sucursal)){
            $crud->where('sucursal',$this->user->sucursal);
        }elseif(is_numeric($sucursal)){
            $crud->where('sucursal',$sucursal);
        }
        $crud->order_by('nombre_comercial','ASC');
        $crud->where("(j286e18ee.anulado IS NULL OR j286e18ee.anulado = '0')",'ESCAPE',TRUE);
        $crud = $crud->render();
        $this->loadView($crud);

    }

    public function inventario_modal_compras($sucursal = ''){
        if($sucursal == 'ajax_list_info'){
            echo '{"total_results":100}';
            return;
        }
        $crud = new ajax_grocery_crud();
        $crud->set_table('productos')
             ->set_subject('Inventario')
             ->set_theme('bootstrap')
             ->unset_add()
             ->unset_edit()
             ->unset_delete()
             ->unset_print()
             ->unset_export()
             ->unset_read()             
             ->callback_column('codigo',function($val,$row){
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
             });
            $crud->columns('codigo','codigo_interno','nombre_comercial','nombre_generico','stock','precio_venta');
            $crud->callback_column('codigo', function($val, $row) {                
                return '<a href="javascript:selCod(\''.$val.'\',\'\','.str_replace('"',"'",json_encode($row)).')">'.$val.'</a>';
            }); 
            $crud->callback_column('precio_venta', function($val, $row) {
                return number_format((float)$val,0,',','.');
            });
            $crud->where("(productos.anulado IS NULL OR productos.anulado = '0')",'ESCAPE',TRUE);
            $crud->set_primary_key('codigo','productos');
            $crud = $crud->render();
            $this->loadView($crud);
    }
    
    public function transferencias($x = '', $y = '',$z = '') {
        redirect('movimientos/transferencias/transferencias');
    }

    public function transferencias_detalles($x = '') {        
            $crud = parent::crud_function('','');
            $crud->set_theme('bootstrap');
            $crud->unset_delete()
                 ->unset_add()
                 ->unset_edit()
                 ->unset_export()
                 ->unset_read()
                 ->unset_print()
                 ->set_primary_key('codigo','productos')
                 ->set_relation('producto','productos','{codigo}|{nombre_comercial}|{nombre_generico}')
                 ->columns('codigo','nombre_comercial','nombre_generico','cantidad')
                 ->callback_column('codigo',function($val,$row){return @explode('|',$row->s286e18ee)[0];})
                 ->callback_column('nombre_comercial',function($val,$row){return @explode('|',$row->s286e18ee)[1];})
                 ->callback_column('nombre_generico',function($val,$row){return @explode('|',$row->s286e18ee)[2];});
            $crud->where('transferencia',$x);
            $crud->staticShowList = true;
            $output = $crud->render();
            echo $output->output;
    }

    public function productosucursal_ainsert($post, $id) {
        $this->db->update('productosucursal', array('precio_venta' => $post['precio_venta']), array('producto' => $post['producto']));
        $this->db->update('productos', array('precio_venta' => $post['precio_venta']), array('codigo' => $post['producto']));
    }
    /* Cruds */

    function productos_costos($prodId){
        $crud = $this->crud_function("","");
        $crud->where('productos_id',$prodId)
             ->field_type('productos_id','hidden',$prodId)
             ->unset_columns('productos_id')
             ->set_relation('ingredientes_id','productos','{codigo} {nombre_comercial}',array('ingrediente'=>1));                
        $crud = $crud->render();
        $crud->header = new ajax_grocery_crud();
        $crud->header = $crud->header->set_table('productos')
               ->set_subject('productos')
               ->set_theme('header_data')
               ->columns('codigo','nombre_generico','nombre_comercial')
               ->where('productos.id',$prodId)
               ->render('1')
               ->output;         
        $crud->title = "Adm. Costos de productos";
        $crud->output = $this->load->view('productos_costos',array('crud'=>$crud),TRUE);
        $this->loadView($crud);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
