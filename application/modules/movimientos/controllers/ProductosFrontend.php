<?php

require_once APPPATH.'/controllers/Panel.php';    

class ProductosFrontend extends Main {

    function __construct() {
        parent::__construct();        
    }     
    
    public function productos($x = '', $y = '') {
        if(!empty($_POST)){
            $this->db->select('
                codigo,
                codigo_interno,
                nombre_comercial,
                precio_venta AS _precio_venta,
                CONCAT(FORMAT(precio_venta,0,"de_DE")," ","Gs.") as precio_venta,
                CONCAT(FORMAT(precio_venta_mayorista1,0,"de_DE")," ","Gs.") as precio_venta_mayorista1,
                CONCAT(FORMAT(precio_venta_mayorista2,0,"de_DE")," ","Gs.") as precio_venta_mayorista2,
                CONCAT(FORMAT(precio_venta_mayorista3,0,"de_DE")," ","Gs.") as precio_venta_mayorista3,                
                cant_1,
                cant_2,
                cant_3'
            );
            $this->db->where('codigo',$_POST['producto']);
            $ci = !is_numeric($_POST['producto'])?-1:$_POST['producto'];
            $this->db->or_where('codigo_interno',$ci);
            $producto = $this->db->get_where('productos');

            if($producto->num_rows()>0){
                $producto = $producto->row();
                $producto->precio_dolares = number_format($producto->_precio_venta/$this->ajustes->tasa_dolares,2,',','.');
                $producto->precio_reales = number_format($producto->_precio_venta/$this->ajustes->tasa_reales,2,',','.');
                $producto->precio_pesos = number_format($producto->_precio_venta/$this->ajustes->tasa_pesos,2,',','.');
                echo json_encode($producto);
            }else{
                echo json_encode([]);
            }
            return;
        }
        $this->loadView([
        	'view'=>'panel',
        	'crud'=>'user',
        	'output'=>$this->load->view('consultarProducto',[],TRUE)
        ]);
    }
}