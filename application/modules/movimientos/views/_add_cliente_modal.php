<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="guardarCliente(this); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Agregar cliente</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		<div class="row" style="margin-left: 0; margin-right: 0">

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Cedula</label>
				    <input type="text" class="form-control" name="cedula" placeholder="Cedula">
				  </div>
				</div>	

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#CI/RUC</label>
				    <input type="text" class="form-control" name="nro_documento" placeholder="CI O RUC">
				  </div>
				</div>				

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre del Cliente</label>
				    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
				  </div>
				</div>

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Apellido del Cliente</label>
				    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
				  </div>
				</div>

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Celular del Cliente</label>
				    <input type="text" class="form-control" name="celular" placeholder="Celular del cliente">
				  </div>				
				</div>

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre de Contacto</label>
				    <input type="text" class="form-control" name="nombre_contacto" placeholder="Nombre de Contacto">
				  </div>				
				</div>

				<div class="col-12 col-md-8">
					<div class="form-group">
					    <label for="exampleInputEmail1">Dirección</label>
					    <input type="text" class="form-control" name="direccion" placeholder="Dirección del cliente">
					  </div>
				</div>

				

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#Documento de Contacto</label>
				    <input type="text" class="form-control" name="cedula_contacto" placeholder="Cédula de contacto">
				  </div>				
				</div>

				<div class="col-6 col-md-4">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Celular de Contacto</label>
				    <input type="text" class="form-control" name="celular_contacto" placeholder="Celular de contacto">
				  </div>				
				</div>

				<div class="col-12 col-md-8">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#Dirección de Contacto</label>
				    <input type="text" class="form-control" name="direccion_contacto" placeholder="Cédula de contacto">
				  </div>				
				</div>

				<div class="col-12 col-md-12">
					<div class="resultClienteAdd"></div>
			  </div>
				</div>			  
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="mayorista" value="1">
	      	<input type="hidden" name="zonas_id" value="1">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<script>

  window.afterLoad.push(function(){
      $(document).on('keydown',"input[name='cedula']",function(e){
        if(e.which==13){
          e.preventDefault();
          $(this).trigger('change');
          return false;
        }
    });

  }); 
	
</script>

<script>
  window.afterLoad.push(function(){  

  	$("#addCliente").on("show.bs.modal",function(){      
      $(".resultClienteAdd").html('').removeClass('alert alert-info alert-success alert-danger');
    });  
    $("#addCliente").on("shown.bs.modal",function(){
      $('#addCliente input[type="text"]').val('');
      $('#addCliente input[name="cedula"]').focus();      
    });

    $("#addCliente").on("hidden.bs.modal",function(){         
      setTimeout(function(){$("#codigoAdd").focus();},600);
    });

    $('#addCliente input[name="cedula"]').on('change',function(){
    	if($(this).val()!=''){
    		$.post('<?= base_url() ?>maestras/ruc_py/json_list',{
    			'search_text[]':$(this).val(),
    			'search_field[]':'cedula',
    			'operator':'where'
    		},function(data){
    			data = JSON.parse(data);
    			if(data.length>0){
    				data = data[0];
    				$('#addCliente input[name="nombres"]').val(data.nombres);
    				$('#addCliente input[name="apellidos"]').val(data.apellidos);
    				$('#addCliente input[name="nro_documento"]').val(data.ruc);
    				
    			}
    		});
    	}
    });


  });

  function guardarCliente(el){
  	insertar('maestras/clientes/insert',el,'.resultClienteAdd',function(data){
  		$('#addCliente input[type="text"]').val('');
  		setCliente(data);
  	});
  }
</script>