<form action="" method="post" onsubmit="insertarUnidad(this); return false;">
	<div class="modal fade" id="unidades" tabindex="-1" role="dialog">
	    <div class="modal-dialog modal-lg" role="document">
	        <div class="modal-content">
	            <div class="modal-header">	                
	                <h4 class="modal-title">Añadir unidad</h4>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            </div>
	            <div class="modal-body">                
	            	<div class="form-group" id="denominacion_field_box">
	                	<label for="field-denominacion" id="denominacion_display_as_box" style="width:100%">
	                		Denominacion<span class="required">*</span>  :
	                	</label>
	                	<input id="field-denominacion" name="denominacion" class="form-control denominacion" type="text" value="" maxlength="40">
	                </div>
	           		<div id="report-error"></div>
	            </div>
	            <div class="modal-footer">
	            	<button type="submit" class="btn btn-success">Guardar</button>        
	                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
	            </div>
	        </div><!-- /.modal-content -->
	    </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</form>

<script>
	function insertarUnidad(obj){
		$('#unidades #report-error').removeClass('alert alert-success alert-danger').html('');
		insertar('maestras/unidad_medida/insert',obj,'',function(data){
			if(data.success){
				$('#unidades').modal('toggle');
				$.post('<?= base_url() ?>maestras/unidad_medida/json_list',{per_page:1000},function(medidas){
					var opt = '';
					medidas = JSON.parse(medidas);
					for(var i in medidas){
						var selected = data.insert_primary_key==medidas[i].id?'selected="true"':'';
						opt+= '<option value="'+medidas[i].id+'" '+selected+'>'+medidas[i].denominacion+'</option>';
					}
					$("select[name='unidad_medida_id']").html(opt);
					$("select[name='unidad_medida_id']").chosen().trigger('liszt:updated');
				});
			}else{
				$('#unidades #report-error').addClass('alert alert-danger').html(data.error_message);
			}
		},function(data){$('#unidades #report-error').addClass('alert alert-danger').html(data.error_message);});
	}
</script>