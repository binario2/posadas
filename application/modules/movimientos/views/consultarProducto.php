<style>
	div.banderas{
		background-image:url('https://dev.binario.com.py/sysventas/img/banderas.jpg');
		width:20px;
			height:20px;
		position: absolute;
		right:0px;
		top:0px;
			background-size: 100%;
		}
	div.banderas.us{
		background-position: 0 0px;
	}
	div.banderas.ar{
		background-position: 0 -20px;
			}
	div.banderas.br{
		background-position: 0 -40px;
	}
	div.banderas.py{
		background-position: 0 -60px;
	}
	div.banderas-2x{
		background-image:url('https://dev.binario.com.py/sysventas/img/banderas.jpg');
		width:40px;
			height:40px;
		position: absolute;
		right:0px;
		top:0px;
			background-size: 100%;
		}
	div.banderas-2x.us{
		background-position: 0 0px;
	}
	div.banderas-2x.ar{
		background-position: 0 -40px;
			}
	div.banderas-2x.br{
		background-position: 0 -80px;
	}
	div.banderas-2x.py{
		background-position: 0 -120px;
	}

	.form-control-plaintext{
		text-align: right
	}
</style>
<div class="row">
	<div class="col-3">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h1 class="kt-portlet__head-title">
							<b>Consultar</b>
							</h1>
						</div>
					</div>
					
					<div>
						<form action="movimientos/productosFrontend/productos" onsubmit="consultar(this,'.response');return false;">
							<div class="form-group">
								<label for="producto">Código de producto</label>
								<input type="text" class="form-control" id="producto" name="producto">
								<small id="emailHelp" class="form-text text-muted">Ingresa el código del producto para consultarlo.</small>
							</div>
							<button type="submit" class="btn btn-primary">Consultar</button>
							<div class="response" style="margin-top:50px"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6 oculto">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h1 class="kt-portlet__head-title">
							<b>Descripción</b>
							</h1>
							<span style="position: absolute;right: 0;font-size: 18px;">
								<b>Código interno: </b>
								<span id="codigoInterno" ></span>
							</span>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12">							
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Código: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 8px 20px;" type="text" name="codigo" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>

							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Nombre: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 0px 22px;" type="text" name="nombre" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Precio: </label>
								<div class="col-sm-11">
									<input style="background: #5867dd;color: #fff;text-align: right;padding: 0px 22px;font-size: 40px;" type="text" name="precio" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
							<div class="form-group row" style="border-top: 1px solid #ebedf2;">
								<label for="staticEmail" class="col-sm-1 col-form-label">Dolares: </label>
								<div class="col-sm-11">
									<input  type="text" name="dolares" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Pesos: </label>
								<div class="col-sm-11">
									<input type="text" name="pesos" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-1 col-form-label">Reales: </label>
								<div class="col-sm-11">
									<input type="text" name="reales" readonly="" class="form-control-plaintext" id="staticEmail" value="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-3 oculto">
		<div class="row">
			<div class="col-12">
				<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h1 class="kt-portlet__head-title">
							<b>Inf. Mayorista</b>
							</h1>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12">
							<div class="form-group row">
								<label class="col-sm-3 col-form-label precioMayorista1">Mayorista 1: </label>
								<div class="col-sm-9">
									<input type="text" readonly="" name="precio_mayorista1" class="form-control-plaintext" id="staticEmail" value="" style="font-size:27px">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label precioMayorista2">Mayorista 2: </label>
								<div class="col-sm-9">
									<input type="text" readonly="" name="precio_mayorista2" class="form-control-plaintext" id="staticEmail" value="" style="font-size:27px">
								</div>
							</div>
							<div class="form-group row">
								<label class="col-sm-3 col-form-label precioMayorista3">Mayorista 3: </label>
								<div class="col-sm-9">
									<input type="text" readonly="" name="precio_mayorista3" class="form-control-plaintext" id="staticEmail" value="" style="font-size:27px">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
			<div class="col-12">
		<div class="kt-portlet">
			<div class="kt-portlet__body" style="padding:10px 25px;">
				<div class="kt-section">
					<div class="kt-portlet__head">
						<div class="kt-portlet__head-label">
							<h1 class="kt-portlet__head-title">
								<b>Cotizaciones</b>
							</h1>
						</div>
					</div>
					
					<div class="row">
						<div class="col-12">							
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-2 col-form-label">Dolar: </label>
								<div class="col-sm-10">
									<input type="text" readonly="" class="form-control-plaintext" id="staticEmail" value="<?= number_format($this->ajustes->tasa_dolares,0,',','.'); ?> Gs">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-2 col-form-label">Pesos: </label>
								<div class="col-sm-10">
									<input type="text" readonly="" class="form-control-plaintext" id="staticEmail" value="<?= number_format($this->ajustes->tasa_pesos,0,',','.'); ?> Gs">
								</div>
							</div>
							<div class="form-group row">
								<label for="staticEmail" class="col-sm-2 col-form-label">Reales: </label>
								<div class="col-sm-10">
									<input type="text" readonly="" class="form-control-plaintext" id="staticEmail" value="<?= number_format($this->ajustes->tasa_reales,0,',','.'); ?> Gs">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
		</div>
		
	</div>
	
</div>
<script>
	function consultar(f){
		var cod = $("input[name='producto']").val();
		if(cod!=''){
			$.post('<?= base_url('movimientos/productosFrontend/productos') ?>',{producto:cod},function(data){
				data = JSON.parse(data);				
				if(typeof data.codigo!='undefined'){
					$("input[name='codigo']").val(data.codigo);
					$("input[name='nombre']").val(data.nombre_comercial);
					$("input[name='precio']").val(data.precio_venta);
					$("input[name='dolares']").val(data.precio_dolares);
					$("input[name='pesos']").val(data.precio_pesos);
					$("input[name='reales']").val(data.precio_reales);					
					$("input[name='precio_mayorista1']").val(data.precio_venta_mayorista1);
					$("input[name='precio_mayorista2']").val(data.precio_venta_mayorista2);
					$("input[name='precio_mayorista3']").val(data.precio_venta_mayorista3);
					$('input[name="producto"]').val('');
					$('input[name="producto"]').attr('placeholder','');
					$("#codigoInterno").html(data.codigo_interno);
				}else{
					$("input").val('');
					$('input[name="producto"]').val('');
					$('input[name="producto"]').attr('placeholder','Producto no encontrado');
				}				
			});		
		}
	}
	window.afterLoad.push(function(){
		$('body').attr('class','kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-aside--minimize');
		$("input[name='producto']").focus();
		$("body").on('click',function(){
			$("input[name='producto']").focus();
		});
	});
</script>
<!--End::Dashboard 1-->