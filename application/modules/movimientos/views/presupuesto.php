<style>
	.panel{
		border-radius:0px;
	}

	.error{
		border: 1px solid red !important;
	}

	.patternCredito{
		background: #ffffffa6;width: 100%;height: 100%;position: absolute;top: 0px;left: 0px;
	}
	.btnsaldo{
		position: absolute;top: 0;right: 15px;
	}
	@media screen and (max-width:768px){
		.btnsaldo{
			top: -15px;
			font-size:20px;
		}	
	}
	.kt-portlet{
		margin:0;
	}

	.table thead th, .table td{
		padding:6px;
	}

	tbody .form-control{
		height:22px;
	}

	.kt-portlet .kt-portlet__head{
		min-height: inherit;
		padding:10px;
	}
</style>
<script>
<?php 
	if(!empty($edit)){
		$venta = $this->querys->getPresupuesto($edit);
		if($venta){
			echo 'var editar = '.json_encode($venta).';';
		}else{
			echo 'var editar = undefined;';
		}
	}else{
		echo 'var editar = undefined;';
	}
?>
</script>
<div class="kt-portlet">
	<div class="kt-portlet__body">
		<div class="kt-section">
			<div class="row">
				<div class="col-12 col-md-9">
					<div class="row">
						<div class="col-12 col-md-4">
							Cliente <a href="#addCliente" data-toggle="modal" style="color:green"><i class="fa fa-plus"></i></a>: 
							<?php 
								$this->db->limit(1);
								if(isset($venta)){
									$this->db->where('id',$venta->cliente);
								}
								echo form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',1,'id="cliente"') 
							?>
							<a href="javascript:saldo()" class="btnsaldo"><i class="fa fa-credit-card"></i></a>
						</div>
						<div class="col-12 col-md-4">
							Transacción: 
							<?php 
								echo form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,'id="transaccion"');
							?>
						</div>
						<div class="col-12 col-md-4">
							Forma de Pago: 
							<?php 
								echo form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',1,'id="formapago"')
							?>
						</div>
						<div class="col-12 col-md-4">
							Vendedor: 
							<?php 
								echo form_dropdown_from_query('usuario','user','id','nombre apellido',$this->user->id,'id="usuario"')
							?>
						</div>
						<div class="col-12 col-md-8">
							Observaciones: 
							<input type="text" name="observaciones" id="observacion" class="form-control">
						</div>
					</div>
					
				</div>
				<div class="col-12 col-md-3" style="position: relative;">
					<span style="position: absolute;top: 26px;left: 30px;font-weight: bold;">Total Presupuesto: </span>
					<input type="text" name="total_presupuesto" class="form-control" readonly="" value="300.000" style="font-size:30px; font-weight:bold; text-align: right;height: 89px;vertical-align: baseline;margin-top: 19px;">
				</div>

			</div>
		</div>

	</div>
</div>

<div class="row">
	<div class="col-12 col-md-9">
		<div class="kt-portlet">
			<div class="kt-portlet__body">
				<div class="kt-section">
					<div style="height:350px; overflow-y:auto">
						<table class="table table-bordered" id="ventaDescr">
							<thead>
								<tr>
									<th>Código</th>
									<th>Nombre</th>
									<th>Cantidad</th>
									<th>Precio</th>							
									<th>Total</th>
									<th>Stock</th>
								</tr>
							</thead>
							<tbody>

								<tr id="productoVacio">
									<td>
										<a href="javascript:void(0)" class="rem" style="display:none;color:red">
											<i class="fa fa-times"></i>
										</a> 
										<span>&nbsp;</span>
									</td>
									<td>&nbsp;</td>
									<td><input name="cantidad" class="cantidad form-control" type="text" style="display:none; width:50px;text-align: right;padding: 0 6px;" value="0"></td>
									<td><input name="precio" class="precio form-control" type="text" style="display:none; width:90px;text-align: right;padding: 0 6px;" value="0"></td>							
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>

								
							</tbody>
						</table>
					</div>

					<div class="row">
						<div class="col-12 col-md-2" style="text-align: center;padding: 6px;">
							Cant: <span id="cantidadProductos">4</span>					
						</div>
						<div class="col-12 col-md-10" style="position: relative;">
							<i class="fa fa-search" data-toggle="modal" data-target="#inventarioModal" style="color: #0fcb0f;font-weight: bold;position: absolute;top: 10px;left: 21px;cursor:pointer;"></i>
							<input id="codigoAdd" type="text" class="form-control" placeholder="Código de producto" style="padding-left: 25px;padding-right: 73px;">
							<button style="position: absolute;top: 1px;right: 10px;padding: 8px;" class="btn btn-primary insertar insertar">Insertar</button>

							<div id="searchProductShort" style="display:none; background: #fff;width: 86%;position: absolute;z-index: 1;border: 1px solid gray;"><ul><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li><li style="list-style: none;"><a href="#">7842323343234 Producto de origen desconocido 1</a></li></ul></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-12 col-md-3" style="padding-left: 5px;">
		<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						Resumen de presupuesto
					</h1>
				</div>
			</div>
			<div class="kt-portlet__head">
				<div class="row">
					<div class="col-12 col-md-4">Pesos: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_pesos" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-12 col-md-4">Reales: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_reales" value="300.000" readonly="" style="width: 100%;">
					</div>
					<div class="col-12 col-md-4">Dolares: </div>
					<div class="col-8" style="margin-bottom:5px">						
						<input type="text" class="form-control" name="total_dolares" value="300.000" readonly="" style="width: 100%;">
					</div>
				</div>
			</div>			
		</div>

		<!--<div class="kt-portlet">
			<div class="kt-portlet__head">
				<div class="kt-portlet__head-label">
					<h1 class="kt-portlet__head-title">
						<b>Atajos</b>
					</h1>
				</div>
			</div>
			<div class="kt-portlet__body">
				<span>(ALT+C) <small>Enfocar busqueda por código</small></span><br/>
				<span>(ALT+I) <small>Mostrar busqueda avanzada</small></span><br/>
				<span>(ALT+P) <small>Procesar venta</small></span><br/>
				<span>(ALT+N) <small>Nueva venta</small></span><br/>
				<span>(ALT+B) <small>Consultar saldo del cliente</small></span><br/>
			</div>
		</div>-->
	

		
	</div>
</div>
<div class="row">
	<div class="col-12">
		<div class="respuestaVenta"></div>
	</div>
</div>
<div class="row">
	<div class="col-12 col-md-9">
		<div class="btn-block btn-group-justified" role="group" aria-label="...">
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" onclick="nuevaVenta();" style="color:#000 !important"><i class="fa fa-plus-circle"></i> Nuevo</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-primary" onclick="sendVenta()"><i class="fa fa-floppy-o"></i> Guardar</button>
		  </div>
		  <div class="btn-group" role="group">
		    <button type="button" class="btn btn-default" <?= empty($edit)?'disabled="true"':'' ?> onclick="<?= empty($edit)?'':'imprimir('.$edit.')' ?>" style="color:#000 !important"><i class="fa fa-print"></i> Imprimir</button>
		  </div>
		  <?php if(!empty($edit)): ?>
	  	   <div class="btn-group" role="group">
		    <a href="<?= base_url('movimientos/ventas/ventas/add/Presupuesto/'.$edit) ?>" class="btn btn-default" style="color:#000 !important"><i class="fa fa-print"></i> Facturar</a>
		  </div>
		  <?php endif ?>
		</div>
	</div>
</div>

<div id="addCliente" class="modal fade" tabindex="-1" role="dialog">
  <form onsubmit="insertar('maestras/clientes/insert',this,'.resultClienteAdd',function(data){setCliente(data)}); return false;">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">	        
	        <h4 class="modal-title">Agregar cliente</h4>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		  <div class="row" style="margin-left: 0; margin-right: 0">
  		  		<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Nombre</label>
				    <input type="text" class="form-control" name="nombres" placeholder="Nombre del cliente">
				  </div>
				</div>
				

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Apellido</label>
				    <input type="text" class="form-control" name="apellidos" placeholder="Apellido del cliente">
				  </div>
				</div>

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">#Documento</label>
				    <input type="text" class="form-control" name="nro_documento" placeholder="Documento del cliente">
				  </div>
				</div>

				<div class="col-12 col-md-6">
				  <div class="form-group">
				    <label for="exampleInputEmail1">Celular</label>
				    <input type="text" class="form-control" name="celular" placeholder="Celular del cliente">
				  </div>				
				</div>

				<div class="col-12 col-md-12">
					<div class="form-group">
					    <label for="exampleInputEmail1">Dirección</label>
					    <input type="text" class="form-control" name="direccion" placeholder="Dirección del cliente">
					  </div>
				</div>

				<div class="col-12 col-md-12">
					<div class="resultClienteAdd"></div>
			    </div>
			</div>			  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <button type="submit" class="btn btn-primary">Guardar</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
  </form>
</div><!-- /.modal -->

<div id="saldo" class="modal fade" tabindex="-1" role="dialog">  
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Saldo del cliente</h4>
	      </div>
	      <div class="panel-body">
	  		  <h1 id="saldoTag" style="text-align: center">2.000GS</h1>		  
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>	        
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php get_instance()->js[] = '<script src="'.base_url().'js/presupuestos.js?v='.time().'"></script>'; ?>
<?php $this->load->view('_inventario_modal',array(),FALSE,'movimientos'); ?>
<script>
	<?php
		$ajustes = $this->ajustes;
	?>
	var ajustes = <?= json_encode(sqltojson($ajustes)) ?>;
	var tasa_dolar = <?= $ajustes->tasa_dolares ?>;
	var tasa_real = <?= $ajustes->tasa_reales ?>;
	var tasa_peso = <?= $ajustes->tasa_pesos ?>;
	var codigo_balanza = <?= $ajustes->cod_balanza ?>;
	var cantidades_mayoristas = <?php 
        $ar = array();
        foreach($this->db->get('cantidades_mayoristas')->result() as $c){
            $ar[] = $c;
        }
        echo json_encode($ar);
	?>;
	var vender_sin_stock = true;
	var onsend = false;

	window.afterLoad.push(function(){
		window.venta = new PuntoDeVenta();	
		if(editar!=undefined){
			console.log(editar);
			venta.setDatos(editar);
		}else{
			venta.datos.usuario = '<?= $this->user->id ?>';
		}
		
		initDatos();
		venta.initEvents();
		venta.updateFields();
	});
	function initDatos(){
		venta.datos.sucursal = '<?= $this->user->sucursal ?>';
		venta.datos.caja = '<?= $this->user->caja ?>';
		venta.datos.cajadiaria = '<?= $this->user->cajadiaria ?>';
		venta.datos.fecha = '<?= date("Y-m-d H:i") ?>';	
		//venta.datos.usuario = '<?= $this->user->id ?>';		
	}

	function imprimir(codigo){
		var url = 'reportes/rep/verReportes/<?= $this->ajustes->id_reporte_presupuestos ?>/html/presupuesto_id/';
        <?php if(empty($edit)): ?>
        window.open('<?= base_url() ?>'+url+codigo);
        <?php else: ?>
    	window.open('<?= base_url() ?>'+url+<?= $edit ?>);
        <?php endif ?>
	}

	function selCod(codigo){		
		venta.addProduct(codigo);		
		$("#inventarioModal").modal('toggle');
	}

	function nuevaVenta(){		
		venta.initVenta();
		initDatos();
		$(".respuestaVenta").html('').removeClass('alert alert-info alert-danger alert-success');		
		if($("#procesar").css('display')=='block'){
			$("#procesar").modal('toggle');
		}
		$(".btnNueva").hide();
		$("button").attr('disabled',false);
		$("#transaccion").val(1);
		$("#transaccion").chosen().trigger('liszt:updated');
		$.post(URI+'maestras/clientes/json_list',{   
            search_field:'id',     
            search_text:'1',
            operator:'where'
        },function(data){       
            data = JSON.parse(data);   
            venta.selectClient(data);
        });
		onsend = false;
	}

	
	function sendVenta(){
		//if(typeof(editar)=='undefined'){
			if(!onsend){
				onsend = true;
				var datos =  JSON.parse(JSON.stringify(venta.datos));
				datos.productos = JSON.stringify(datos.productos);
				var accion = typeof(editar)=='undefined'?'insert':'update/'+editar.id;
				$("button").attr('disabled',true);
				insertar('movimientos/presupuesto/presupuesto/'+accion,datos,'.respuestaVenta',function(data){						
					var id = data.insert_primary_key;						
					var enlace = '';
					$(".respuestaVenta").removeClass('alert-info');
					imprimir(id);
					enlace = 'javascript:imprimir('+id+')';
					$('.respuestaVenta').removeClass('alert alert-danger').addClass('alert alert-success').html(data.success_message+'<p>La factura se mostrará automáticamente, si no lo hace puede pulsar <a href="'+enlace+'">este enlace</a></p>');
					$(".btnNueva").show();
					$("button").attr('disabled',false);				
					onsend = false;
				},function(){
					onsend = false;
					$("button").attr('disabled',false);				
				});
			}
		/*}else{
			alert('Edición no permitida');
		}*/
	}

	function setCliente(data){
		if(data.success){
			$(".resultClienteAdd").html('<div class="alert alert-success">Cliente añadido con éxito</div>');
			$.post(URI+'maestras/clientes/json_list',{   
	            search_field:'id',     
	            search_text:data.insert_primary_key,
	            operator:'where'
	        },function(data){       
	            data = JSON.parse(data);   
	            venta.selectClient(data);
	        });
		}
	}

	function saldo(){
		$.post(base_url+'movimientos/ventas/saldo',{cliente:$("#cliente").val()},function(data){
			$("#saldoTag").html(data);
			$("#saldo").modal('toggle');
		});
	}
</script>