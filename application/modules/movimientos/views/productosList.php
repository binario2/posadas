<?php echo $output ?>
<script>
	function masDetalles(codigo){
		window.afterLoad = [];
		$.get('<?= base_url() ?>movimientos/productos/productos/masDetalles/'+codigo,{},function(data){			
			$("body").append(data);
			$("body").find('#masdetalles').modal('toggle');
			$("body").find('#masdetalles').on('hidden.bs.modal',function(){
				$("body").find('#responseMasDetalles').remove();
			});
			for(var i in window.afterLoad){
				window.afterLoad[i]();
			}
		});
	}
</script>