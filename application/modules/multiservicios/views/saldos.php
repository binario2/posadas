<?php 

$qry = "
	SELECT
		sc.id as Id_cuenta,
		sc.denominacion as Cuenta,
		(ifnull(tacreditacion.total,0)+ifnull(tretirogiro.total_retiro_giro,0)+ifnull(tapositivo.total_ajuste_positivo,0))-(ifnull(tenviogiro.total_envio_giro,0)+ifnull(tcargagiro.total_carga_giro,0)+ifnull(ttelefonia.total_telefonia,0)+ifnull(tanegativo.total_ajuste_negativo,0)) as saldo
		FROM servicio_cuentas sc
		LEFT JOIN (
		SELECT 
		sa.servicio_cuentas_id as ci,
		sum(sa.monto) as total
		FROM servicio_acreditaciones sa
		WHERE (sa.anulado = 0 or sa.anulado is null)
		GROUP BY sa.servicio_cuentas_id
		) as tacreditacion on tacreditacion.ci = sc.id
		LEFT JOIN (
		SELECT 
		servicio_giros.servicio_cuentas_id as ci,
		ifnull(sum(servicio_giros.monto),0) as total_retiro_giro
		FROM servicio_giros
		INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
		WHERE (servicio_giros.anulado = 0 or servicio_giros.anulado is null) and servicio_cuentas.calcular_saldo = 1 and servicio_giros.servicio_tipo_transacciones_id = 1
		GROUP BY servicio_giros.servicio_cuentas_id

		) as tretirogiro on tretirogiro.ci = sc.id
		LEFT JOIN (
		SELECT 
		servicio_giros.servicio_cuentas_id as ci,
		ifnull(sum(servicio_giros.monto),0) as total_envio_giro
		FROM servicio_giros
		INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
		WHERE (servicio_giros.anulado = 0 or servicio_giros.anulado is null) and servicio_cuentas.calcular_saldo = 1 and servicio_giros.servicio_tipo_transacciones_id = 2
		GROUP BY servicio_giros.servicio_cuentas_id 
		) as tenviogiro on tenviogiro.ci = sc.id
		LEFT JOIN (
		SELECT 
		servicio_giros.servicio_cuentas_id as ci,
		ifnull(sum(servicio_giros.monto),0) as total_carga_giro
		FROM servicio_giros
		INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
		WHERE (servicio_giros.anulado = 0 or servicio_giros.anulado is null) and servicio_cuentas.calcular_saldo = 1 and servicio_giros.servicio_tipo_transacciones_id = 3
		GROUP BY servicio_giros.servicio_cuentas_id
		) as tcargagiro on tcargagiro.ci = sc.id
		LEFT JOIN (
		SELECT 
		st.servicio_cuentas_id as ci,
		ifnull(sum(st.monto),0) as total_telefonia
		FROM servicio_telefonia st WHERE (st.anulado = 0 or st.anulado is null)
		GROUP BY st.servicio_cuentas_id
		) as ttelefonia on ttelefonia.ci = sc.id
		LEFT JOIN (
		SELECT 
		ajs.servicio_cuentas_id as ci,
		ifnull(sum(ajs.monto),0) as total_ajuste_positivo 
		FROM ajuste_servicio_cuentas ajs WHERE ajs.tipo_ajuste = '+'
		GROUP BY ajs.servicio_cuentas_id

		) as tapositivo on tapositivo.ci = sc.id
		LEFT JOIN (

		SELECT 
		ajs.servicio_cuentas_id as ci,
		ifnull(sum(ajs.monto),0) as total_ajuste_negativo 
		FROM ajuste_servicio_cuentas ajs WHERE ajs.tipo_ajuste = '-'
		GROUP BY ajs.servicio_cuentas_id

		) as tanegativo on tanegativo.ci = sc.id
		WHERE sc.calcular_saldo = 1
";

$qry = $this->db->query($qry);

sqlToHtml($qry);