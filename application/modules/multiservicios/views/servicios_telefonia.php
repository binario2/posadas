<?php 
	echo $output 
?>

<script>
	$('#field-saldo_anterior,#field-monto').attr('readonly',true);
	$(document).on('change',"#field-servicio_mov_general_id,#field-servicio_empresas_id",function(){
		if($("#field-servicio_mov_general_id").val()!='' && $("#field-servicio_empresas_id").val()!=''){
			$.post('<?= base_url() ?>multiservicios/servicio_cuentas/json_list',{
				'serv_mov_general':$("#field-servicio_mov_general_id").val(),				
				'serv_empresa':$("#field-servicio_empresas_id").val()
			},function(data){
				data = JSON.parse(data);
				var opt = '<option>Seleccione una opcion</option>';
				for(var i in data){
					opt+= '<option value="'+data[i].id+'">'+data[i].denominacion+'</option>';
				}
				$("#field-servicio_cuentas_id").html(opt);
				$("#field-servicio_cuentas_id").chosen().trigger('liszt:updated');
			});
		}
	});

	$(document).on('change','#field-servicio_cuentas_id',function(){
		$.post('<?= base_url() ?>multiservicios/servicio_telefonia/getSaldo',{
			'scid':$(this).val()
		},function(data){
			data = JSON.parse(data);
			$("#field-saldo_anterior").val(data.saldo);
		});
	});

	$(document).on('change','#field-saldo_actual',function(){
		var total = parseFloat($('#field-saldo_anterior').val()) - parseFloat($('#field-saldo_actual').val());
		$("#field-monto").val(total);
	});
</script>