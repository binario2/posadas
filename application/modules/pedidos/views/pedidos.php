<?= $output ?>
<script>
    var todos = $("#mesas_id_field_box,#parrillas_id_field_box,#mozos_id_field_box,#campings_id_field_box,#habitaciones_id_field_box");
    var parrilla = $("#parrillas_id_field_box");
    var mozos = $("#mozos_id_field_box");
    var campings = $("#campings_id_field_box");  
    var habitaciones = $("#habitaciones_id_field_box");  
    var mesas = $("#mesas_id_field_box");  
    window.afterLoad.push(function(){
        mostrar();
        $("#field-tipo_pedidos_id").on('change',function(){
            mostrar();
        });
    });
    
    function mostrar(){
        switch($("#field-tipo_pedidos_id").val()){
            case '1': //Mozos
                todos.hide();
                habitaciones.show();
                mozos.show();
            break;
            case '2': //parrilla
                todos.hide();
                parrilla.show();
            break;
            case '3': //campings
                todos.hide();
                campings.show();
            break;
            case '4': //campings
                todos.hide();
                mesas.show();
            break;
            case '5': //deliverys
                todos.hide();
                deliverys.show();
            break;
            default:
                todos.hide();
            break;
        }
        $(".chosen-container").css('width','100%');
    }
</script>