<div id='list-report-error' class='alert alert-danger' style="display:none;"></div>
<div id='list-report-success' class='alert alert-success' <?php if ($success_message !== null) { ?>style="display:block"<?php } else { ?> style="display:none" <?php } ?>><?php if ($success_message !== null) { ?>
        <p><?php echo $success_message; ?></p>
    <?php }
    ?></div>

<?php echo form_open($ajax_list_url, 'method="post" id="filtering_form" class="filtering_form" autocomplete = "off" data-ajax-list-info-url="' . $ajax_list_info_url . '" onsubmit="return filterSearchClick(this)"'); ?>
<div class="flexigrid" style='width: 100%;' data-unique-hash="<?php echo $unique_hash; ?>">    
    






    <div class="kt-portlet kt-portlet--mobile">        
        <div>
            <div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--loaded" style="width:100%; overflow: auto">
            <!--begin: Datatable -->
                <table class="table table-striped table-bordered table-hover table-checkable">
                    <thead class="table-active">
                        <tr>
                            <?php foreach ($columns as $column): ?>
                                <th class="field-sorting sorting" rel='<?php echo $column->field_name ?>'>
                                    <?php echo $column->display_as ?>                        
                                    <span id="th_<?= $column->field_name ?>"></span>
                                    <?php if (isset($order_by[0]) && $column->field_name == $order_by[0]) { ?>
                                        <?php if ($order_by[1] == 'asc'): ?>
                                            <i class="fa fa-arrow-up"></i>                        
                                        <?php else: ?>
                                            <i class="fa fa-arrow-down"></i>
                                        <?php endif; ?> 
                                    <?php } ?>
                                </th>
                            <?php endforeach ?>

                            <?php if (!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)): ?>
                                <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                                    <div class="text-right">
                                        <?php echo $this->l('list_actions'); ?>
                                    </div>
                                </th>
                            <?php endif ?>
                        </tr>

                        <tr class="searchRow">  
                            <?php foreach($columns as $column):?>
                            <th>
                                <?php if(!in_array($column->field_name,$unset_searchs)): ?>
                                    <input type="hidden" name="search_field[]" value="<?= $column->field_name ?>">
                                    <?php
                                       $value = '';
                                       if(!empty($_POST['search_text']) && !empty($_POST['search_field'])){
                                           foreach($_POST['search_field'] as $n=>$v){
                                               if($v==$column->field_name && !empty($_POST['search_text'][$n])){
                                                   $value = $_POST['search_text'][$n];
                                               }
                                           }
                                       }
                                       if(empty($search_types[$column->field_name])){
                                            echo form_input('search_text[]',$value,'style="width:100%" class="form-control" placeholder="Filtrar por '.$column->display_as.'"');
                                       }else{
                                            echo $search_types[$column->field_name];
                                       }
                                    ?>
                                <?php endif ?>
                            </th>
                            <?php endforeach?>                
                        </tr>
                    </thead>
                    <tbody class="ajax_list">
                        <?php if (!empty($list)): ?>
                        <?php echo $list_view?>
                        <?php else: ?>
                            <tr><td colspan="<?= count($columns)+2 ?>">Consultando</td></tr>
                        <?php endif ?>
                    </tbody>
                </table>

                <div class="kt-datatable__pager kt-datatable--paging-loaded">
                    
                    <div class="kt-datatable__pager-info">
                        <div class="dropdown bootstrap-select kt-datatable__pager-size" style="width: auto;">
                            <button title='Refrescar listado'  type="button" class="ajax_refresh_and_loading btn btn-default btn-icon-sm">
                                <span>
                                    <i class="ace-icon fa fa-sync bigger-110 grey"></i>
                                </span>
                            </button>
                            <input type="hidden" name="per_page" id='per_page' value="<?php echo $default_per_page ?>">                            
                        </div>
                        <span class="kt-datatable__pager-detail">
                            <?php $paging_starts_from = "<span id='page-starts-from' class='page-starts-from'>1</span>"; ?>
                            <?php $paging_ends_to = "<span id='page-ends-to' class='page-ends-to'>" . ($total_results < $default_per_page ? $total_results : $default_per_page) . "</span>"; ?>
                            <?php $paging_total_results = "<span id='total_items' class='total_items'>$total_results</span>" ?>
                            <?php echo str_replace(array('{start}', '{end}', '{results}'), array($paging_starts_from, $paging_ends_to, $paging_total_results), $this->l('list_displaying')); ?>
                        </span>
                    </div>
                    <ul class="pagination kt-datatable__pager-nav">
                    </ul>
                </div>
            </div>
            <!--end: Datatable -->
        </div>
    </div>















</div>
<input type='hidden' name='order_by[0]' class='hidden-sorting' value='<?php if (!empty($order_by[0])) { ?><?php echo $order_by[0] ?><?php } ?>' />
<input type='hidden' name='order_by[1]' class='hidden-ordering'  value='<?php if (!empty($order_by[1])) { ?><?php echo $order_by[1] ?><?php } ?>'/>
<?php echo form_close() ?>
<?php if(!$unset_import): ?>
    <?php include $this->default_theme_path . '/bootstrap/views/import.php'; ?>
<?php endif ?>  