<?php if($pedidos->facturado==1): ?>
    <div class="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i> Pedido ya facturado por lo que no se pueden realizar cambios en el mismo.
    </div>
<?php else: ?>        
    <form action="" onsubmit="return send(this)">
        <div class="card">
            <div class="card-header">
                Datos del cliente
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-sm-3">
                        <?php $this->db->order_by("nombres"); ?>
                        <b>Cliente: </b> <a href="#" title="Añadir cliente" data-toggle="modal" data-target="#addCliente"><i class="fa fa-plus-circle"></i></a>
                        <?php $cliente = empty($venta->clientes_id)?1:$venta->clientes_id; ?>
                        <?= form_dropdown_from_query('cliente','clientes','id','nro_documento nombres apellidos',$cliente,'id="cliente"',FALSE) ?>
                    </div>
                    <div class="col-12 col-sm-3">
                        <b>Transacción: </b><?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',1,"",FALSE); ?>
                    </div>
                    <div class="col-12 col-sm-3">
                        <b>Forma de pago: </b><?= form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',1,"",FALSE); ?>
                    </div>
                    <div class="col-12 col-sm-3">
                        <b>Tipo de facturación: </b><?= form_dropdown_from_query('tipo_facturacion_id',$this->db->get('tipo_facturacion'),'id','denominacion',1,"",FALSE); ?>
                    </div>
                    
                    <div class="col-12" style="position: absolute;right: 10px;top: -22px;">
                        <?php $factura = $this->querys->get_nro_factura(); ?>
                        <input type="hidden" name="nro_factura" value="">
                        <p align="right"><b>Factura #</b><span id="nrofactura"></span></p>
                    </div>
                    <div class="col-12">
                        <table class="table table-bordered">
                            <thead>
                                <tr><th>Producto</th><th>Cantidad</th><th>Importe</th></tr>
                            </thead>
                            <tbody id="listado">
                                <tr id="emptylist" class=""><td colspan="3">Seleccione los productos en la pestaña <b>Pedido</b></td></tr>
                                <tr id="productlist" class="d-none">
                                    <td>{nombre}</td>
                                    <td>                            
                                        <input type="text" id="cantidadInput" class="form-control retrato" value="0">
                                    </td>
                                    <td>
                                        <input type="text" id="totalInput" class="form-control retrato" value="0" readonly=""> 
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top:30px">
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Detalle del pedido
                    </div>
                    <div>
                        <table class="table table-bordered mb-0">                                          
                            <tr><th><b>Monto Abonado</b></th><td><b><?= number_format($pedidos->abonado,0,',','.') ?> Gs</b></td></tr>                            
                            <tr><th>Productos</th><td id="productos">0</td></tr>
                            <tr><th>Productos elegidos</th><td id="productosElegidos">0</td></tr>
                            <tr><th>Importe total</th><td><span id="importeTotal"></span> Gs</td></tr>
                            <tr><th>Importe a facturar</th><td><b><span id="importeAFacturar">0</span> Gs</b></td></tr>
                            <tr><th>Importe pendiente</th><td><span id="importePendiente">0</span> Gs</td></tr>
                            <tr><th>Pendiente por cancelar</th><td><span id="PendientePorCancelar">0</span> Gs</td></tr>
                            <tr><th>Efectivo a cobrar</th><td><span id="EfectivoACobrar">0</span> Gs</td></tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-6">
                <div class="card">
                    <div class="card-header">
                        Detalle del pago
                    </div>
                    <div>
                        <table class="table table-bordered mb-0">                
                            <tr><th>Efectivo</th><td><input type="text" name="total_efectivo" class="form-control pago" value="0"></td></tr>
                            <tr><th>Vuelto</th><td><input type="text" name="vuelto" class="form-control vuelto" value="0" readonly=""></td></tr>
                            <tr><th>Débito</th><td><input type="text" name="total_debito" class="form-control pago" value="0"></td></tr>
                            <tr><th>Crédito</th><td><input type="text" name="total_credito" class="form-control pago" value="0"></td></tr>
                            <tr><th>Cheque</th><td><input type="text" name="total_cheque" class="form-control pago" value="0"></td></tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div style="text-align: right">
            <input type="hidden" name="total_venta" value="0">
            <input type="hidden" name="total_iva" value="0">
            <input type="hidden" name="iva" value="0">
            <input type="hidden" name="iva2" value="0">
            <input type="hidden" name="exenta" value="0">
            <input type="hidden" name="total_dolares" value="0">
            <input type="hidden" name="total_reales" value="0">
            <input type="hidden" name="sucursal" value="<?= $this->user->sucursal ?>">
            <input type="hidden" name="caja" value="<?= $this->user->caja ?>">
            <input type="hidden" name="cajadiaria" value="<?= $this->user->cajadiaria ?>">
            <input type="hidden" name="usuario" value="<?= $this->user->id ?>">
            <input type="hidden" name="status" value="0">
            <input type="hidden" name="pedidos_id" value="<?= $pedido ?>">
            <input type="hidden" name="fecha" value="<?= date("Y-m-d H:i:s") ?>">
            <div id="messagebox" style="text-align:left;"></div>
            <button class="btn btn-success" type="submit" id='cargarFactura'>Cargar Factura</button>
        </div>
    </form>
<?php endif ?>