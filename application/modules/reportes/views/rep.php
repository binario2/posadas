<?php get_instance()->hcss[] = '<style>
	.chzn-drop,.chzn-drop input{
		width:100% !important;
	}
</style>'; ?>
<?php echo $output ?>
<?php get_instance()->js[] = '
	<script src="'.base_url().'printer/zip/zip.js"></script>
	<script src="'.base_url().'printer/zip/zip-ext.js"></script>
	<script src="'.base_url().'printer/zip/deflate.js"></script>
	<script src="'.base_url().'printer/scripts/JSPrintManager.js"></script>
'; ?>
<script>
	var clientPrinters = null;
 	window.afterLoad.push(function(){
	    //WebSocket settings
	    JSPM.JSPrintManager.auto_reconnect = true;
	    JSPM.JSPrintManager.start();
	    JSPM.JSPrintManager.WS.onStatusChanged = function () {
	        if (jspmWSStatus()) {
	            //get client installed printers   
	            JSPM.JSPrintManager.getPrintersInfo().then(function (printersList) {
	            	clientPrinters = printersList;
	            });         
	            JSPM.JSPrintManager.getPrinters().then(function (myPrinters) {	            	
	            	var val = $('#field-impresora').val();
	                var options = '<select id="field-impresora" name="impresora" class="form-control">';
	                options += '<option value="">Método imprimir del navegador</option>';
	                if(val!=''){
	                	options += '<option value="'+val+'">' + val + '</option>';
	                }
	                for (var i = 0; i < myPrinters.length; i++) {
	                	var sel = val==myPrinters[i]?'selected="true"':'';
	                    options += '<option value="'+myPrinters[i]+'" '+sel+'>' + myPrinters[i] + '</option>';
	                }
	                options+= '</select>';
	                $('#field-impresora').replaceWith(options);
	                $(document).on('change','#field-impresora',function(){showSelectedPrinterInfo();});
	                if($("#field-impresora").val()!=''){
	                	showSelectedPrinterInfo();
	                }
	                console.log(myPrinters);
	            });
	        }
	    };
	});

	//Check JSPM WebSocket status
    function jspmWSStatus() {
        if (JSPM.JSPrintManager.websocket_status == JSPM.WSStatus.Open){
            return true;
        }
        return false;
    }

    //Opciones de la impresora seleccionada
    function showSelectedPrinterInfo() {
        // get selected printer index
        var idx = $("#field-impresora")[0].selectedIndex;
        // get supported trays
        var options = '';
        for (var i = 0; i < clientPrinters[idx].trays.length; i++) {
            options += '<option>' + clientPrinters[idx].trays[i] + '</option>';
        }
        $('#field-impresora_bandeja').html(options);
        $('#field-impresora_bandeja').chosen().trigger('liszt:updated');
        // get supported papers
        options = '';
        for (var i = 0; i < clientPrinters[idx].papers.length; i++) {
            options += '<option>' + clientPrinters[idx].papers[i] + '</option>';
        }
        $('#field-impresora_papel').html(options);
        $('#field-impresora_papel').chosen().trigger('liszt:updated');
        console.log(idx,clientPrinters,options);
    }
</script>