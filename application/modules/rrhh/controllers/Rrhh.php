<?php

require_once APPPATH.'/controllers/Panel.php';    

class Rrhh extends Panel {

    function __construct() {
        parent::__construct();             
    }
        
    public function empleados($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);  
        $crud->set_theme('bootstrap2_horizontal');  
        $crud->set_relation('periodo_pago','periodo_pago','{denominacion}');
        $crud->set_relation_n_n('puestos','puestos_empleados','puestos_montos','empleado_id','puestos_montos_id','puesto');
        if(!empty($_POST['id'])){
        	$crud->where('empleados.id',$_POST['id']);
            $crud->callback_column('monto_salario',function($val,$row){
                $this->db->select('SUM(puestos_montos.monto) as total');
                $this->db->join('puestos_montos','puestos_montos.id = puestos_empleados.puestos_montos_id');
                $puestos = $this->db->get_where('puestos_empleados',['empleado_id'=>$row->id]);
                if($puestos->num_rows()>0){
                    $val+= $puestos->row()->total;
                }
                return $val;
            });
        }
        $output = $crud->render();
        $this->loadView($output);
    }

    function getAsignaciones($empleado){
    	$pagos = [
			'sanciones'=>0,
			'saldo_credito'=>0,
			'otros_ingresos'=>0,
			'bonificaciones'=>0,
			'horas_extras'=>0,
			'adelanto_empleados'=>0
		];
		if(empty($_POST['desde']) || empty($_POST['hasta'])){
			echo json_encode($pagos);
			return;
		}
		$desde = $_POST['desde'];
		$hasta = $_POST['hasta'];
		//Sanciones 
		$saldo = $this->db->query("SELECT IFNULL(SUM(monto),0) as total FROM sansion_empleados WHERE empleados_id = '{$empleado}' AND fecha_sancion BETWEEN '$desde' AND '$hasta'");
		$pagos['sanciones'] = $saldo->num_rows()>0?$saldo->row()->total:0;

		//otros_ingresos
		$saldo = $this->db->query("SELECT IFNULL(SUM(monto),0) as total FROM otros_ingresos_empleados WHERE empleados_id = '{$empleado}' AND fecha BETWEEN '$desde' AND '$hasta'");
		$pagos['otros_ingresos'] = $saldo->num_rows()>0?$saldo->row()->total:0;

		//horas_extras
		$saldo = $this->db->query("SELECT IFNULL(SUM(total_monto),0) as total FROM horas_extras WHERE empleados_id = '{$empleado}' AND fecha BETWEEN '$desde' AND '$hasta'");
		$pagos['horas_extras'] = $saldo->num_rows()>0?$saldo->row()->total:0;

		//adelanto_empleados
		$saldo = $this->db->query("SELECT IFNULL(SUM(monto),0) as total FROM adelanto_empleados WHERE empleados_id = '{$empleado}' AND fecha_adelanto BETWEEN '$desde' AND '$hasta'");
		$pagos['adelanto_empleados'] = $saldo->num_rows()>0?$saldo->row()->total:0;
		

		//Creditos
		$saldo = $this->db->query("
			SELECT 
			facturas_clientes.saldo_cliente as total
			FROM empleados 
			INNER JOIN user ON user.id = empleados.user_id
			LEFT JOIN clientes ON clientes.user_id = empleados.user_id
			LEFT JOIN facturas_clientes ON facturas_clientes.clientes_id = clientes.id
			WHERE empleados.id = '{$empleado}'
		");
		$pagos['saldo_credito'] = $saldo->num_rows()>0?$saldo->row()->total:0;
		echo json_encode($pagos);
    }

    public function pago_empleados($x = '', $y = ''){
    	if($x=='getAsignaciones' && is_numeric($y)){
    		$this->getAsignaciones($y);
    		return;
    	}
        if($x=='pagar' && is_numeric($y)){
            $this->db->update('pago_empleados',['estado'=>1,'cajadiaria_pagado'=>$this->user->cajadiaria,'fecha_pagado'=>date("Y-m-d H:i:s")],['id'=>$y]);
            redirect('reportes/rep/verReportes/130/html/valor/'.$y);
            return;
        }
        $crud = parent::crud_function($x, $y); 
        $crud->set_subject('Pagos');
        $crud->set_theme('bootstrap2_horizontal');
        $crud->set_primary_key('id','view_empleados');
        $crud->set_relation('empleados_id','view_empleados','{nombre} {apellido}');
        if($this->user->admin==0 && in_array(3,$this->user->getGroupsArray())){
            $crud->unset_edit()
                 ->unset_delete()
                 ->unset_print()
                 ->unset_export()
                 ->unset_read()
                 ->unset_add();
        }
        $crud->display_as('empleados_id','Empleado')
        	 ->display_as('periodo_pago_id','Periodo')
        	 ->field_type('user_registro','hidden',$this->user->id)
        	 ->field_type('anulado','hidden',0)
             ->field_type('cajadiaria_pagado','hidden',0)
             ->field_type('estado','hidden',0)
             ->field_type('fecha_pagado','hidden','')
        	 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
        	 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])
        	 ->field_type('forma_pago','enum',['Efectivo','Cheque','Transferencia','Deposito']) 
             ->unset_delete()
             ->set_relation('bancos_id','bancos','denominacion')
             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id')
             ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';});
        $crud->callback_field('desde',function($val){return '<input type="date" name="desde" id="field-desde" class="form-control">';})
        	 ->callback_field('hasta',function($val){return '<input type="date" name="hasta" id="field-hasta" class="form-control">';})
             ->callback_before_insert(function($post){
                $post['estado'] = 0;
                return $post;
             })
        	 ->callback_after_insert(function($post,$primary){
                //Insertar en libreo de banco
                if($post['forma_pago'] == 'Cheque' || $post['forma_pago']=='Transferencia' || $post['forma_pago']=='Deposito'){                    
                    $saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']])->row()->saldo;
                    $empleado = $this->db->get_where('view_empleados',['id'=>$post['empleados_id']])->row();
                    $this->db->insert("libro_banco",[
                        'sucursales_id'=>$this->user->sucursal,
                        'cajas_id'=>$this->user->caja,
                        'cajadiaria_id'=>$this->user->cajadiaria,
                        'fecha'=>date("Y-m-d H:i:s"),
                        'bancos_id'=>$post['bancos_id'],
                        'cuentas_bancos_id'=>$post['cuentas_bancos_id'],
                        'tipo_movimiento'=>-1,
                        'saldo_actual'=>$saldo_actual,
                        'concepto'=>'Pago a empleado',
                        'beneficiario'=>$empleado->nombre.' '.$empleado->apellido,
                        'monto'=>$post['total_a_cobrar'],
                        'comprobante'=>$primary,
                        'forma_pago'=>$post['forma_pago'],
                        'tipo_cheque'=>$post['tipo_cheque'],
                        'nro_cheque'=>$post['nro_cheque'],
                        'fecha_emision'=>date("Y-m-d H:i:s"),
                        'fecha_pago'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_pago']))),
                        'cobrado'=>0,
                        'anulado'=>0,
                        'user_id'=>$this->user->id,
                        'fecha_registro'=>date("Y-m-d H:i:s")
                        
                    ]);                    
                }                
            });
        if(is_numeric($x)){
            $crud->where('estado',$x);
        }else{
            $crud->where('estado = 0','ESCAPE');
            $crud->add_action('Pagar','',base_url('rrhh/pago_empleados/pagar').'/');
        }
        $crud->unset_columns('user_registro','forma_pago','s589074de','sc227f205','nro_cheque','tipo_cheque','fecha_emision','cajadiaria','anulado');
        $crud->set_lang_string('insert_success_message','Datos almacenados con éxito <a href="javascript:imprimir({id})">Imprimir recibo</a> <script>imprimir({id});</script> | ');
        $output = $crud->render();
        $output->output = $this->load->view('pago_empleados',['x'=>$x,'y'=>$y,'output'=>$output->output],TRUE);
        $output->title = 'Pago a empleados';
        $this->loadView($output);
    }

    public function horas_extras($x = '', $y = ''){
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Horas Extras');      
        $crud->set_theme('bootstrap2_horizontal');          
        if(!empty($_POST['empleado'])){
        	$crud->where('horas_extras.empleados_id',$_POST['empleado']);
        }
        $crud->set_primary_key('id','view_empleados');
        $crud->set_relation('empleados_id','view_empleados','{nombre} {apellido}');
        $crud->display_as('empleados_id','Empleado')
        	 ->display_as('periodo_pago_id','Periodo')
        	 ->field_type('user_registro','hidden',$this->user->id)
        	 ->field_type('anulado','hidden',0)
        	 ->field_type('cajadiaria','hidden',$this->user->cajadiaria);
        $output = $crud->render();
        $output->output.= $this->load->view('horas_extras',[],TRUE);
        $output->title = "Horas Extras";
        $this->loadView($output);
    }

    public function sanciones($x = '', $y = ''){
    	$this->as['sanciones'] = 'sansion_empleados';
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Sanciones');      
        $crud->set_theme('bootstrap2_horizontal');          
        if(!empty($_POST['empleado'])){
        	$crud->where('horas_extras.empleados_id',$_POST['empleado']);
        }
        $crud->set_primary_key('id','view_empleados');
        $crud->set_relation('empleados_id','view_empleados','{nombre} {apellido}');
        $crud->display_as('empleados_id','Empleado')
        	 ->display_as('periodo_pago_id','Periodo')
        	 ->field_type('user_registro','hidden',$this->user->id)
        	 ->field_type('anulado','hidden',0)
        	 ->field_type('cajadiaria','hidden',$this->user->cajadiaria);
        $output = $crud->render();
        $output->title = "Sanciones";
        $this->loadView($output);
    }

    public function otros_ingresos_empleados($x = '', $y = ''){    	
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Otros Ingresos');      
        $crud->set_theme('bootstrap2_horizontal');          
        if(!empty($_POST['empleado'])){
        	$crud->where('otros_ingresos_empleados.empleados_id',$_POST['empleado']);
        }
        $crud->set_primary_key('id','view_empleados');
        $crud->set_relation('empleados_id','view_empleados','{nombre} {apellido}');
        $crud->display_as('empleados_id','Empleado')
        	 ->display_as('periodo_pago_id','Periodo')
        	 ->field_type('user_carga','hidden',$this->user->id)
        	 ->field_type('anulado','hidden',0)
        	 ->field_type('cajadiaria','hidden',$this->user->cajadiaria);
        $output = $crud->render();
        $output->title = "Otros Ingresos";
        $this->loadView($output);
    }

    public function adelanto_empleados($x = '', $y = ''){    	
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Anticipos');      
        $crud->set_theme('bootstrap2_horizontal');          
        if(!empty($_POST['empleado'])){
        	$crud->where('adelanto_empleados.empleados_id',$_POST['empleado']);
        }
        $crud->set_primary_key('id','view_empleados');
        $crud->set_relation('empleados_id','view_empleados','{nombre} {apellido}');
        $crud->display_as('empleados_id','Empleado')        	 
        	 ->field_type('user_registro','hidden',$this->user->id)
        	 ->field_type('anulado','hidden',0)
        	 ->field_type('cajadiaria','hidden',$this->user->cajadiaria)
        	 ->field_type('tipo_cheque','dropdown',['1'=>'A la vista','-1'=>'Diferido'])
        	 ->field_type('forma_pago','enum',['Efectivo','Cheque','Transferencia','Deposito']) 
             ->set_relation('bancos_id','bancos','denominacion')
             ->set_relation('cuentas_bancos_id','cuentas_bancos','nro_cuenta')
             ->set_relation_dependency('cuentas_bancos_id','bancos_id','bancos_id')
             ->callback_field('fecha_emision',function($value,$row){$value = empty($value)?date("Y-m-d"):$value; return '<input type="date" name="fecha_emision" id="field-fecha_emision" class="form-control" value="'.$value.'">';})
             ->callback_after_insert(function($post,$primary){
                //Insertar en libreo de banco
                if($post['forma_pago'] == 'Cheque' || $post['forma_pago']=='Transferencia' || $post['forma_pago']=='Deposito'){                    
                    $saldo_actual = $this->db->get_where('cuentas_bancos',['id'=>$post['cuentas_bancos_id']])->row()->saldo;
                    $empleado = $this->db->get_where('view_empleados',['id'=>$post['empleados_id']])->row();
                    $this->db->insert("libro_banco",[
                        'sucursales_id'=>$this->user->sucursal,
                        'cajas_id'=>$this->user->caja,
                        'cajadiaria_id'=>$this->user->cajadiaria,
                        'fecha'=>date("Y-m-d H:i:s"),
                        'bancos_id'=>$post['bancos_id'],
                        'cuentas_bancos_id'=>$post['cuentas_bancos_id'],
                        'tipo_movimiento'=>-1,
                        'saldo_actual'=>$saldo_actual,
                        'concepto'=>'Adelanto de pago',
                        'beneficiario'=>$empleado->nombre.' '.$empleado->apellido,
                        'monto'=>$post['monto'],
                        'comprobante'=>$primary,
                        'forma_pago'=>$post['forma_pago'],
                        'tipo_cheque'=>$post['tipo_cheque'],
                        'nro_cheque'=>$post['nro_cheque'],
                        'fecha_emision'=>date("Y-m-d H:i:s"),
                        'fecha_pago'=>date("Y-m-d H:i:s",strtotime(str_replace('/','-',$post['fecha_adelanto']))),
                        'cobrado'=>0,
                        'anulado'=>0,
                        'user_id'=>$this->user->id,
                        'fecha_registro'=>date("Y-m-d H:i:s")
                    ]);                    
                }                
            });
        $output = $crud->render();
        $output->title = "Anticipos";
        $output->output.= $this->load->view('adelantos',[],TRUE);
        $this->loadView($output);
    }

    public function puestos_montos($x = '', $y = ''){       
        $crud = parent::crud_function($x, $y);    
        $crud->set_subject('Puestos de trabajo');      
        $crud->set_theme('bootstrap2_horizontal');
        $output = $crud->render();
        $output->title = "Puestos de trabajo";        
        $this->loadView($output);
    }

    



    
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
