#función para actualizar stock desde after_insert_compradetalles ID de compradetalles
#ajust_stock_from_entrada_detalle PARAMS EntradaDetalle INT
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE varVencimiento DATE;
DECLARE control_stock INT;
DECLARE varInventariable INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	#Traemos el producto
	SELECT 
		entrada_productos_detalles.producto, 
		entrada_productos_detalles.sucursal, 
		entrada_productos_detalles.cantidad, 
		entrada_productos_detalles.vencimiento,
		productos.inventariable 
	INTO 
		varProduct,varSucursal,
		varCantidad,
		varVencimiento,
		varInventariable 
	FROM entrada_productos_detalles 
	INNER JOIN productos ON productos.codigo = entrada_productos_detalles.producto 
	WHERE entrada_productos_detalles.id = EntradaDetalle LIMIT 1;

	IF varInventariable = 1 THEN
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		IF varProductoSucursalId IS NULL THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, varVencimiento, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		END IF;

		#Actualizamos el stock
		UPDATE productosucursal SET stock = (stock+varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	END IF;
#END IF;
END