#función para actualizar stock desde after_insert_salida_detalle ID de salida_detalle
#ajust_stock_from_salida_detalle PARAMS SalidaDetalle INT
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE control_stock INT;
DECLARE varInventariable INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	#Traemos el producto
	
	SELECT 
		salida_detalle.producto,
		salida_detalle.sucursal,
		salida_detalle.cantidad,
		productos.inventariable 
	INTO 
		varProduct,
		varSucursal,
		varCantidad,
		varInventariable 
	FROM salida_detalle 
	INNER JOIN productos ON productos.codigo = salida_detalle.producto 
	WHERE salida_detalle.id = SalidaDetalle;

	IF varInventariable = 1 THEN
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		IF varProductoSucursalId IS NULL THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, varVencimiento, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		END IF;

		#Actualizamos el stock
		UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	END IF;
#END IF;
END