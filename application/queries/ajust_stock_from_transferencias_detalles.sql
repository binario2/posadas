#función para actualizar stock desde after_insert_transferencias_detalles ID de compradetalles
#ajust_stock_from_transferencias_detalles PARAMS TransferenciasDetalle INT
BEGIN
DECLARE varInventariable INT;
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);

DECLARE varProduct2 VARCHAR(255);
DECLARE varSucursal2 INT;
DECLARE varProductoSucursalId2 INT;
DECLARE varCantidad2 DECIMAL(11,2);

DECLARE control_stock INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	##SUCURSAL ORIGEN
	
	#Traemos el producto
	SELECT 
		transferencias_detalles.producto,
		transferencias.sucursal_origen,
		transferencias_detalles.cantidad,
		productos.inventariable 
	INTO 
		varProduct,
		varSucursal,
		varCantidad,
		varInventariable 
	FROM transferencias_detalles 
	INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia 
	INNER JOIN productos ON productos.codigo = transferencias_detalles.producto 
	WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;

	IF varInventariable = 1 THEN
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		IF varProductoSucursalId IS NULL THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, NULL, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		END IF;

		#Actualizamos el stock
		UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;

		##SUCURSAL DESTINO
		#Traemos el producto
		SELECT transferencias_detalles.producto,transferencias.sucursal_destino,transferencias_detalles.cantidad INTO varProduct2,varSucursal2,varCantidad2 FROM transferencias_detalles INNER JOIN transferencias ON transferencias.id = transferencias_detalles.transferencia WHERE transferencias_detalles.id = TransferenciasDetalle LIMIT 1;
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 LIMIT 1;
		IF varProductoSucursalId2 IS NULL THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal SELECT NULL, proveedor_id, NOW(), codigo, varSucursal2, NULL, NULL, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId2 FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal2 LIMIT 1;
		END IF;

		#Actualizamos el stock
		UPDATE productosucursal SET stock = (stock+varCantidad2) WHERE productosucursal.id = varProductoSucursalId2;
	END IF;
#END IF;
END