#función para actualizar stock desde after_insert_ventadetalle ID de ventadetalle
#PARAMS VentaDetalle INT
BEGIN
DECLARE varProduct VARCHAR(255);
DECLARE varSucursal INT;
DECLARE varProductoSucursalId INT;
DECLARE varCantidad DECIMAL(11,2);
DECLARE control_stock INT;
DECLARE varInventariable INT;
DECLARE varProductoID INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	
	#Traemos el producto
	SELECT 
		producto,
		ventas.sucursal,
		ventadetalle.cantidad,
		productos.inventariable,
		productos.id
	INTO 
		varProduct,
		varSucursal,
		varCantidad,
		varInventariable,
		varProductoID
	FROM ventadetalle 
	INNER JOIN ventas ON ventas.id = ventadetalle.venta 
	INNER JOIN productos ON productos.codigo = ventadetalle.producto 
	WHERE ventadetalle.id = VentaDetalle LIMIT 1;

	IF varInventariable = 1 THEN
		#Traemos el id de stock
		SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		IF varProductoSucursalId IS NULL THEN
			#Insertamos el producto para comenzar su stockage
			INSERT INTO productosucursal SELECT NULL, proveedor_id, NOW(), codigo, varSucursal, NULL, NULL, precio_venta, precio_costo, 0 FROM productos WHERE BINARY codigo = BINARY varProduct;
			SELECT id INTO varProductoSucursalId FROM productosucursal WHERE BINARY producto = BINARY varProduct AND sucursal = varSucursal LIMIT 1;
		END IF;

		#Actualizamos el stock
		UPDATE productosucursal SET stock = (stock-varCantidad) WHERE productosucursal.id = varProductoSucursalId;
	END IF;	

	#Productos asociados?
	UPDATE productosucursal,(
		SELECT 
		productosucursal.id,
		productos_id,
		descontar,
		cant,
		productosucursal.stock,
		TRUNCATE(productosucursal.stock - cant,2) as newstock
		FROM `producto_asociado`
		INNER JOIN productos ON productos.id = producto_asociado.descontar
		LEFT JOIN productosucursal ON productosucursal.producto = productos.codigo AND productosucursal.sucursal = 1
		WHERE producto_asociado.productos_id = varProductoID
	) as cd
	SET productosucursal.stock = cd.newstock
	WHERE productosucursal.id = cd.id;
#END IF;
END