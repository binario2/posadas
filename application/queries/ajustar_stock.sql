# CALL ajustar_stock(_producto INT, _sucursal INT,_operacion varchar(1),_cantidad INT)
BEGIN
DECLARE xstock INT;
DECLARE control_stock INT;
DECLARE varInventariable INT;

SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
IF(control_stock=0) THEN
    SELECT 
        stock,
        inventariable 
    INTO 
        xstock,
        varInventariable 
    FROM productosucursal 
    INNER JOIN productos ON productos.codigo = productosucursal.producto
    WHERE producto = xproducto AND sucursal = xsucursal;

    IF varInventariable = 1 THEN 
        IF (xstock IS NOT NULL) THEN

            IF xoperacion = '+' THEN
                SET xstock = xstock + xcantidad;
            ELSE 
                SET xstock = xstock - xcantidad;
            END IF;

        END IF;

        UPDATE productosucursal SET stock = xstock WHERE producto = xproducto AND sucursal = xsucursal;
    END IF;
END IF;
END