#Call cerrarCaja(varCajadiaria)
DROP PROCEDURE IF EXISTS cerrarCaja;
DELIMITER $$
CREATE PROCEDURE cerrarCaja(varCajadiaria INT)
	BEGIN
	DECLARE varMonto_inicial INT;
	DECLARE varContado INT;
	DECLARE varPagocliente INT;
	DECLARE varCreditos INT;
	DECLARE varVentas INT;
	DECLARE varTotal_entrega INT;
	DECLARE varCaja INT;
	DECLARE varEfectivoARendir INT;
	DECLARE varFecha_cierre DATE;
	DECLARE varEgresos INT;
	DECLARE varServicios INT;
	DECLARE varTotalReservas INT;

	select IFNULL(SUM(monto),0) INTO varEgresos from gastos where cajadiaria = varCajadiaria;

	SELECT caja INTO varCaja FROM cajadiaria WHERE cajadiaria.id = varCajadiaria;

	SELECT monto_inicial INTO varMonto_inicial FROM cajadiaria WHERE cajadiaria.id = varCajadiaria;

	SELECT IFNULL(SUM(totalcondesc),0) INTO varContado 
	FROM ventadetalle 
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria=varCajadiaria AND status=0 AND transaccion=1;

	SELECT IFNULL(SUM(total_pagado),0) INTO varPagocliente 
	FROM pagocliente
	WHERE (anulado = 0 OR anulado IS NULL) AND pagocliente.cajadiaria = varCajadiaria;

	SELECT IFNULL(SUM(totalcondesc),0) INTO varCreditos
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria = varCajadiaria AND status=0 AND transaccion=2;

	SELECT IFNULL(SUM(totalcondesc),0) INTO varVentas 
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productos ON productos.codigo = ventadetalle.producto
	WHERE cajadiaria=varCajadiaria AND status=0;

	SELECT IFNULL(sum(entrega_inicial),0) INTO varTotal_entrega
	FROM creditos
	inner JOIN ventas on ventas.id = creditos.ventas_id
	WHERE ventas.status != -1 and creditos.cajadiaria =varCajadiaria;

	SELECT IFNULL(sum(monto),0) INTO varTotalReservas
	FROM reservas
	WHERE (reservas.anulado IS NULL OR reservas.anulado = 0) AND reservas.cajadiaria =varCajadiaria;

	SELECT DATE(NOW()) INTO varFecha_cierre;

	SELECT get_saldo_servicios(varCajadiaria) INTO varServicios;

	SELECT (varServicios+varMonto_inicial+varContado+varPagocliente+varTotal_entrega+varTotalReservas) - varEgresos INTO varEfectivoARendir;

	UPDATE cajadiaria 
	SET 
	abierto = 0,
	total_contado = varContado,
	total_pago_clientes = varPagocliente,
	total_credito = varCreditos,
	total_ventas = varVentas,
	total_descuentos = 0,
	total_entrega_credito = varTotal_entrega,
	total_egreso = varEgresos,
	total_servicios = varServicios,
	efectivoarendir = varEfectivoARendir,
	fecha_cierre = varFecha_cierre
	WHERE cajadiaria.id = varCajadiaria;

	UPDATE cajas SET abierto = 0 WHERE cajas.id = varCaja;


END $$