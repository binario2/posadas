#FUNCTION getDV(p_numero)
BEGIN

DECLARE v_total INT;
DECLARE v_resto INT;
DECLARE k INT;
DECLARE v_numero_aux INT;
DECLARE v_numero_al VARCHAR(255);
DECLARE v_caracter VARCHAR(1);
DECLARE v_digit INT;
DECLARE p_basemax INT;
DECLARE i INT;
-- Cambia la ultima letra por ascii en caso que la cedula termine en letra   
SET i:= 1;
SET p_basemax:= 11;

WHILE i < LENGTH(p_numero) DO
	SET v_caracter:= UPPER(SUBSTR(p_numero,i,1));
	IF ASCII(v_caracter) NOT BETWEEN 48 AND 57 THEN  -- de 0 a 9
		SET v_numero_al := v_numero_al || ASCII(v_caracter);
	ELSE 
		SET v_numero_al := v_numero_al || v_caracter;
	END IF;
	SET i:= (i+1);
END WHILE;
-- Calcula el DV  
SET k:= 2;
SET v_total:= 0;

SET i:= LENGTH(v_numero_al);
WHILE i>0 DO
	IF k > p_basemax THEN
		SET k:= 2;
	END IF;
	SET v_numero_aux:= CONVERT(SUBSTR(v_numero_al,i,1),UNSIGNED INTEGER);
	SET v_total:= v_total + (v_numero_aux * k);
	SET k:= k + 1;
	SET i:= (i-1);
END WHILE;
SET v_resto:= MOD(v_total,11);
IF v_resto > 1 THEN
	SET v_digit:= 11 - v_resto;
ELSE
	SET v_digit:= 0;
END IF;
RETURN v_digit;
END