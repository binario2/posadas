BEGIN
DECLARE _saldo INT;
SET @saldo = 0;
SET @deuda = 0;

SELECT get_total_pagado(_cliente) INTO _saldo;

SET @saldo = _saldo;

SELECT 
'TOTAL ABONADO' as producto,
_saldo as totalcondesc,
'' as cubre,
'' as saldo,
'' as residuo,
'' as deuda
UNION
SELECT
producto,
totalcondesc,
cubre,
saldo,
residuo,
@deuda:= @deuda + saldo
FROM (
SELECT 
producto,
totalcondesc,
cubre,
saldo,
@saldo:= TRUNCATE(IF(@saldo>0,@saldo-cubre,0),0) as residuo
FROM 
(
SELECT 
ventadetalle.producto,
ventadetalle.totalcondesc,
IF(@saldo>ventadetalle.totalcondesc,ventadetalle.totalcondesc,TRUNCATE(@saldo,0)) as cubre,
( CASE 
    WHEN @saldo > 0 THEN
		IF(@saldo - ventadetalle.totalcondesc>0,0,ventadetalle.totalcondesc - @saldo)
    ELSE ventadetalle.totalcondesc
    END
) as saldo
FROM ventadetalle 
INNER JOIN ventas ON ventas.id = ventadetalle.venta
WHERE 
ventas.cliente = _cliente AND
ventas.transaccion = 2 AND 
(ventas.status = 0 OR ventas.status IS NULL)
) as extracto) as estrac
UNION 
SELECT
'TOTALES',
(SELECT SUM(ventadetalle.totalcondesc) FROM ventadetalle INNER JOIN ventas ON ventas.id = ventadetalle.venta WHERE ventas.cliente = _cliente AND ventas.transaccion = 2 AND (ventas.status = 0 OR ventas.status IS NULL)),
0,
@deuda,
0,
0;

END