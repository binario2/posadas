#SELECT get_saldo_servicios(_cajadiariaid)
BEGIN
DECLARE dinero INT;
SELECT
sum(arqueo.total) INTO dinero
FROM(
SELECT 
'Giros_Retiro' as Mov,
'Egreso' as Tipo,
servicio_cuentas.denominacion as Cuenta,
ifnull(sum(servicio_giros.monto)*-1,0) as total
FROM servicio_giros
INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
WHERE 
    (servicio_giros.anulado = 0 or servicio_giros.anulado is null) AND 
    servicio_cuentas.calcular_saldo = 1 AND 
    servicio_giros.servicio_tipo_transacciones_id = 1 AND
    servicio_giros.cajadiaria_id = _cajadiaria
GROUP BY servicio_giros.servicio_cuentas_id
UNION all

SELECT 
'Giros_Envio' as Mov,
'Ingreso' as Tipo,
servicio_cuentas.denominacion as Cuenta,
ifnull(sum(servicio_giros.monto),0) as total
FROM servicio_giros
INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
WHERE (servicio_giros.anulado = 0 or servicio_giros.anulado is null) and servicio_cuentas.calcular_saldo = 1 and servicio_giros.servicio_tipo_transacciones_id = 2 AND servicio_giros.cajadiaria_id = _cajadiaria
GROUP BY servicio_giros.servicio_cuentas_id
UNION ALL

SELECT 
'Giros_Carga' as Mov,
'Ingreso' as Tipo,
servicio_cuentas.denominacion as Cuenta,
ifnull(sum(servicio_giros.monto),0) as total
FROM servicio_giros
INNER JOIN servicio_cuentas on servicio_cuentas.id = servicio_giros.servicio_cuentas_id
WHERE (servicio_giros.anulado = 0 or servicio_giros.anulado is null) and servicio_cuentas.calcular_saldo = 1 and servicio_giros.servicio_tipo_transacciones_id = 3 AND servicio_giros.cajadiaria_id = _cajadiaria
GROUP BY servicio_giros.servicio_cuentas_id
UNION ALL

SELECT 
'Telefonia_Carga' as Mov,
'Ingreso' as Tipo,
sc.denominacion as Cuenta,
ifnull(sum(st.monto),0) as total
FROM servicio_telefonia st 
INNER JOIN servicio_cuentas sc on sc.id = st.servicio_cuentas_id
WHERE (st.anulado = 0 or st.anulado is null) AND st.cajadiaria_id = _cajadiaria
GROUP BY st.servicio_cuentas_id
UNION ALL

SELECT
'Servicios_Pagos' as Mov,
'Ingreso' as Tipo,
sc.denominacion as Cuenta,
sum(sw.monto) as total
FROM servicios_web sw
INNER JOIN servicio_cuentas sc on sc.id = sw.servicio_cuentas_id
WHERE (sw.anulado = 0 or sw.anulado is null) and sw.servicio_tipo_transacciones_id =2
GROUP BY sw.servicio_cuentas_id

UNION ALL

SELECT
'Servicios_Retiros' as Mov,
'Egreso' as Tipo,
sc.denominacion as Cuenta,
sum(sw.monto)*-1 as total
FROM servicios_web sw
INNER JOIN servicio_cuentas sc on sc.id = sw.servicio_cuentas_id
WHERE (sw.anulado = 0 or sw.anulado is null) and sw.servicio_tipo_transacciones_id =1
GROUP BY sw.servicio_cuentas_id) as arqueo;
RETURN dinero;
END