#CALL reversar_stock_from_compras after update and anulado = 1 compraID
BEGIN
DECLARE control_stock INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(SELECT 
	productosucursal.id,
	compradetalles.producto,
	compras.sucursal,
	compradetalles.cantidad,
	productosucursal.stock
	FROM compradetalles
	INNER JOIN compras ON compras.id = compradetalles.compra
	INNER JOIN productosucursal ON productosucursal.producto = compradetalles.producto AND productosucursal.sucursal = compras.sucursal
	WHERE compra = compraID) AS compra SET productosucursal.stock = (productosucursal.stock-compra.cantidad) WHERE productosucursal.id = compra.id;
#END IF;
END