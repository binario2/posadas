#CALL reversar_stock_from_salidas after update and anulado = 1 salidaID
BEGIN
DECLARE control_stock INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(SELECT 
	productosucursal.id,
	salida_detalle.producto,
	salida_detalle.sucursal,
	salida_detalle.cantidad,
	productosucursal.stock
	FROM salida_detalle	
	INNER JOIN productosucursal ON productosucursal.producto = salida_detalle.producto AND productosucursal.sucursal = salida_detalle.sucursal
	WHERE salida_detalle.id = salidaID) AS salida SET productosucursal.stock = (productosucursal.stock+salida.cantidad) WHERE productosucursal.id = salida.id;
#END IF;
END