#CALL reversar_stock_from_ventas after update and anulado = 1 ventaID
BEGIN
DECLARE control_stock INT;
SELECT vender_sin_stock INTO control_stock FROM ajustes limit 1;
#IF(control_stock=0) THEN
	UPDATE productosucursal,(
	SELECT 
	productosucursal.id,
	ventadetalle.producto,
	ventas.sucursal,
	ventadetalle.cantidad,
	productosucursal.stock
	FROM ventadetalle
	INNER JOIN ventas ON ventas.id = ventadetalle.venta
	INNER JOIN productosucursal ON productosucursal.producto = ventadetalle.producto AND productosucursal.sucursal = ventas.sucursal
	WHERE venta = ventaID) AS venta SET productosucursal.stock = (productosucursal.stock+venta.cantidad) WHERE productosucursal.id = venta.id;

	#Productosasociados
	UPDATE productosucursal,(
		SELECT 
		productosucursal.id,
		productosucursal.stock,
		producto_asociado.cant,
		asociado.codigo,
		TRUNCATE(productosucursal.stock+producto_asociado.cant,2) as newstock
		FROM ventadetalle
		INNER JOIN ventas ON ventas.id = ventadetalle.venta
		INNER JOIN productos On productos.codigo = ventadetalle.producto
		INNER JOIN producto_asociado ON producto_asociado.productos_id = productos.id
		INNER JOIN productos as asociado ON asociado.id = producto_asociado.descontar
		INNER JOIN productosucursal ON productosucursal.sucursal = ventas.sucursal AND productosucursal.producto = asociado.codigo
		WHERE ventadetalle.venta = ventaID
	) as cd
	SET productosucursal.stock = cd.newstock
	WHERE productosucursal.id = cd.id;
#END IF;
END