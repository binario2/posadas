#CALL updateAllPagos
BEGIN

DECLARE creditos INT;
DECLARE i INT;
DECLARE credito INT;
SELECT COUNT(id) INTO creditos FROM creditos WHERE anulado IS NULL OR anulado = 0;
SET i= 0;

WHILE i<creditos DO
SELECT id INTO credito FROM creditos WHERE anulado IS NULL OR anulado = 0 LIMIT i,1;
CALL updatePagos(credito);
SET i= i+1;
END WHILE;

END