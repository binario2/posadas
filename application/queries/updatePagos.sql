BEGIN

SET @pagado = 0;
UPDATE plan_credito, (SELECT * FROM (
SELECT 
*,
CASE WHEN total_pagado > 0 THEN -1
	 WHEN @pagado >= cuotas THEN 1 ELSE 0
END AS pagado,

CASE WHEN @pagado>0 THEN IF((cuotas - @pagado)<0,0,(cuotas - @pagado)) ELSE cuotas

END as saldo,
@pagado:= (@pagado+total_pagado)-cuotas AS totalpago
FROM(
SELECT
pagocliente.id,
SUM(pagocliente.total_pagado) as total_pagado,
'0' AS cuotas
FROM pagocliente WHERE creditos_id = _credito and (pagocliente.anulado = 0 or pagocliente.anulado is null)

UNION ALL
SELECT 
plan_credito.id,
'0',
plan_credito.monto_a_pagar
FROM plan_credito
WHERE plan_credito.creditos_id = _credito) as saldo) AS cd) AS cd 
SET plan_credito.pagado = cd.pagado, 
	plan_credito.saldo = cd.saldo,
	plan_credito.total_pagado = (cd.cuotas - cd.saldo)
WHERE cd.pagado >= 0 AND plan_credito.id = cd.id;

END