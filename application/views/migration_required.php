<div class="kt-portlet">
	<div class="kt-portlet__body" style="padding:10px 25px;">
		<div class="kt-section">
			<h3 class="text-center">Se requiere actualizar su base de datos para continuar utilizando este sistema</h3 class="text-center">
			<div class="mt-30" style="margin-top:30px"></div>
			<a href="?apply_migration=1" class="btn btn-warning btn-block text-center">Aplicar Actualización</a>
		</div>
	</div>
</div>