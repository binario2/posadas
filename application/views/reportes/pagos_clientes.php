<?php if(empty($_POST)): ?>
<? $this->load->view('predesign/datepicker'); ?>
<? $this->load->view('predesign/chosen'); ?>
<div class="container">
    <h1 align="center"> Pagos de clientes</h1>
<form action="<?= base_url('reportes/pagos_clientes') ?>" method="post">
  <div class="form-group">
    <label for="exampleInputEmail1">Seleccione una sucursal</label>
        <?= form_dropdown_from_query('sucursal','sucursales','id','denominacion',0) ?>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Desde</label>
    <input type="text" name="desde" class="form-control datetime-input" id="desde">
  </div>  
  <div class="form-group">
    <label for="exampleInputPassword1">Hasta</label>
    <input type="text" name="hasta" class="form-control datetime-input" id="hasta">
  </div>
  <button type="submit" class="btn btn-default">Consultar reporte</button>
</form>
</div>
<?php else: ?>
    <? if(!empty($_POST['sucursal']))$sucursal = $this->db->get_where('sucursales',array('id'=>$_POST['sucursal']))->row()->denominacion; ?>
    <h1 align="center"> Listado de pagos</h1>
    <p><strong>Sucursal: </strong> <?= empty($_POST['sucursal'])?'Todos':$sucursal ?></p>
    <p><strong>Desde:</strong> <?= empty($_POST['desde'])?'Todos':date("d/m/Y",strtotime(str_replace('/','-',$_POST['desde']))) ?> <strong>Hasta:</strong> <?= empty($_POST['hasta'])?'Todos':date("d/m/Y",strtotime(str_replace('/','-',$_POST['desde']))) ?></p>
    <?php 
        $this->db->select('
            pagocliente.id,
            clientes.nombres as cliente,
            pagocliente.sucursal,
            pagocliente.caja,
            user.nombre as user,
            pagocliente.totalpagado,
            pagocliente.fecha,
            clientes.nro_documento,
            clientes.apellidos,
            clientes.nombres,
            pagocliente.anulado
            ');
            $this->db->from('pagocliente');            
            $this->db->join('clientes','pagocliente.cliente = clientes.id','inner');
            $this->db->join('user','user.id = pagocliente.user','inner');            
            $this->db->where('anulado',0);
            $this->db->order_by('fecha','ASC');
            if(!empty($_POST['sucursal'])){
                $this->db->where('pagocliente.sucursal',$_POST['sucursal']);
            }
            if(!empty($_POST['desde'])){
                $this->db->where('DATE(pagocliente.fecha) >=',date("Y-m-d",strtotime(str_replace('/','-',$_POST['desde']))));
            }
            
            if(!empty($_POST['hasta'])){
                $this->db->where('DATE(pagocliente.fecha) <=',date("Y-m-d",strtotime(str_replace('/','-',$_POST['hasta']))));
            }
            $data = $this->db->get();
    ?>
    <table class="table" width="100%" style="font-size:12px;">
        <thead>
                <tr>
                        <th style="width:100px">Id Pago</th>
                        <th style="width:300px">Cajero</th>
                        <th style="width:100px">Fecha</th>
                        <th style="width:100px">Hora</th>
                        <th style="width:300px">Cliente</th>
                        <th style="width:300px">Monto</th>
                </tr>
        </thead>
        <tbody>    
            <?php $total = 0; ?>
            <?php foreach($data->result() as $c): ?>
                <tr>                        
                        <td><?= $c->id ?></td>
                        <td><?= $c->user ?></td>
                        <td><?= date("d-m-Y",strtotime($c->fecha)) ?></td>
                        <td><?= date("H:i:s",strtotime($c->fecha)) ?></td>
                        <td><?= $c->cliente ?></td>
                        <td><?= number_format($c->totalpagado,2,',','.'); ?></td>
                        <?php $total+= $c->totalpagado; ?>
                </tr>
            <?php endforeach ?>
                <tr>                        
                        <td colspan="5"><b>Total</b></td>                        
                        <td><?= number_format($total,2,',','.'); ?></td>
                </tr>
        </tbody>
    </table>
<?php endif; ?>