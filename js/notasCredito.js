var NotaCredito = function(){
	this.productoHTML = $("#productoVacio").clone();
	this.productoHTML.removeAttr('id');
	this.productoHTML.find('a,input').show();
	this.productoshtml = $("#ventaDescr tbody");
	this.INP_nro_venta = $("input[name='nro_venta']");
	this.INP_nro_nota = $("input[name='nro_nota_credito']");
	this.INP_cliente = $("input[name='cliente']");
	this.INP_total_nota = $("input[name='total_nota']");
	this.INP_total_dolares = $("input[name='total_dolares']");
	this.INP_total_reales = $("input[name='total_reales']");
	this.INP_total_pesos = $("input[name='total_pesos']");	
	this.INP_codigo = $("#codigoAdd");
	this.INP_motivo_nota = $("select[name='motivo_nota_id']");
	this.INP_tipo_facturacion_nota = $("select[name='tipo_facturacion_nota']");
	this.datos = {};
	this.alt = false;
	this.initDatos = function(){
		this.datos = {
			 	venta:0,
			 	nro_factura:'',
			 	nro_nota_credito:0,
			 	fecha:0,
			 	cliente:0,
			 	clienteNombre:'',
			 	total_monto:0,
			 	actualizar_stock:1,
			 	sucursal:0,
			 	usuario:0,
			 	caja:0,
			 	cajadiaria:0,
			 	productos:[],
			 	total_dolares:0,
			 	total_reales:0,
			 	total_pesos:0,
			 	motivo_nota_id:''
		};
		this.getNroNota();
	}
	
	this.getNroNota = function(){
		let l = this;
		$.post(window.URI+'movimientos/ventas/notas_credito_cliente/getNroNota',function(data){
			l.INP_nro_nota.val(data);
			l.datos.nro_nota_credito = data;
		});
	}

	this.initDatos();
	this.initEvents = function(){
		var l = this;
		$(document).on('change',"input[name='nro_venta']",function(){
			l.INP_nro_venta.removeClass('error');
			l.updateData();
			l.searchVenta();
		});

		

		$(document).on('change','.cantidad',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].producto.codigo==codigo){
                    l.datos.productos[i].cantidad = cantidad;
                }
            }
            l.updateData();
	    });

	    $(document).on('change','.precio',function(){
            var cantidad = parseFloat($(this).val());
            var codigo = $(this).parents('tr').data('codigo');
            for(var i in l.datos.productos){
                if(l.datos.productos[i].producto.codigo==codigo){
                    l.datos.productos[i].precioventa = cantidad;
                }
            }
            l.updateData();
	    });

		//Atajos
	    $(document).on('keydown',function(e){
	    	if(e.which==18){
	    		l.alt = true;
	    	}
	    });
		//Atajos
	    $(document).on('keyup',function(e){
	    	if(e.which==18){
                l.alt = false;
	    	}else{
	            if(l.alt){
	                switch(e.which){
                        case 67: //[c] Focus en codigos
                                $("#codigoAdd").focus();
                        break;
                        case 73: //[i] busqueda avanzada
                                $("#inventarioModal").modal('toggle');		    				
                        break;
                        case 80: //[p] Procesar venta
                                save();		    				
                        break;
                        case 78: //[n] Nueva venta
                                nuevaNota();
                                l.INP_nro_venta.focus();
                        break;
	                }
	            }
	    	}

	    });

	    //Event Remove
	    $(document).on('click','.rem',function(){
	    	l.removeProduct($(this).parents('tr').data('codigo'));
	    });

	    $(document).on('change','',function(){			
			l.updateData();			
		});

		$(document).on('keyup','#codigoAdd',function(e){
	    	if(e.which==13){
	    		//$(this).trigger('change');
	    		l.addProduct($(this).val());
	    	}
	    });

	    $(document).on('click','.insertar',function(){
	    	l.addProduct($('#codigoAdd').val());
	    });
	}



	this.initNota = function(){
		this.initDatos();
		this.draw();
	}

	this.searchVenta =  function(){
		var l = this;
		$.post(base_url+'movimientos/ventas/ventas/json_list',{
			'ventas.id':this.datos.nro_factura
		},function(data){	
			data = JSON.parse(data);		
			if(data.length>0){				
				data = data[0];
				if(data.tipo_facturacion_id=='2' || data.tipo_facturacion_id=='4'){
					l.getNroNota();
				}else{
					l.INP_nro_nota.val(0);
					l.datos.nro_nota_credito = 0;
				}
				$.post(base_url+'movimientos/ventas/ventadetalle/json_list',{
					venta:data.id,
					per_page:1000,
					page:1
				},function(dd){					
					data.productos = JSON.parse(dd);
					l.updateData(data);
					l.INP_codigo.attr('readonly',false);
				});
			}else{
				l.INP_nro_venta.addClass('error');
			}
		});
	};

	this.updateData = function(datos){		
		if(typeof(datos)=='undefined'){
			this.datos.nro_factura = this.INP_nro_venta.val();
			//this.datos.nro_nota_credito = this.INP_nro_nota.val();			
			//this.datos.nro_nota_credito = '0';
		}else{
			this.datos.venta = datos.id;			
			this.datos.cliente = datos.cliente;
			this.datos.productos = datos.productos;
			this.datos.clienteNombre = datos.s4983a0ab;			
		}
		this.datos.motivo_nota_id = this.INP_motivo_nota.val();
		this.datos.actualizar_stock = $("input[name='actualizar']:checked").val();
		var total = 0;
		for(var i in this.datos.productos){
			this.datos.productos[i].cantidad = parseFloat(this.datos.productos[i].cantidad);
			total+= parseFloat(this.datos.productos[i].precioventa) * parseFloat(this.datos.productos[i].cantidad);
		}
		this.datos.total_monto = total;			
		//Calcular divisas
		this.datos.total_dolares = (total/tasa_dolar).toFixed(2);
		this.datos.total_reales = (total/tasa_real).toFixed(2);
		this.datos.total_pesos = (total/tasa_peso).toFixed(2);	
		this.draw();	
	}

	this.draw = function(){
		//console.log(this.datos);
		const gs = new Intl.NumberFormat('es-ES', {style: 'currency',currency: 'PYG',minimumFractionDigits: 0});
		$("#cantidadProductos").html(this.datos.productos.length);
		this.INP_cliente.val(this.datos.clienteNombre);
		this.INP_total_nota.val(this.datos.total_monto);
		this.INP_nro_nota.val(this.datos.nro_nota_credito);
		this.INP_nro_venta.val(this.datos.nro_factura);
		this.INP_total_dolares.val(this.datos.total_dolares);
		this.INP_total_reales.val(this.datos.total_reales);
		this.INP_total_pesos.val(this.datos.total_pesos);			
		this.INP_motivo_nota.val(this.datos.motivo_nota_id).chosen().trigger('liszt:updated');			
		//Draw products
		var pr = this.datos.productos;
		this.productoshtml.html('');		
		for(var i=pr.length-1;i>=0;i--){
			//console.log(pr[i].producto);
			var newRow = this.productoHTML.clone();
			var td = newRow.find('td');
			newRow.attr('data-codigo',pr[i].producto.codigo);
			$(td[0]).find('span').html(pr[i].producto.codigo);
			$(td[1]).html(pr[i].producto.nombre_comercial);
			$(td[2]).find('input').val(pr[i].cantidad);
			$(td[3]).find('input').val(parseFloat(pr[i].precioventa).toFixed(0));						
			$(td[4]).html(gs.format(parseFloat(pr[i].precioventa * pr[i].cantidad)));
			this.productoshtml.append(newRow);			
		}
	}

	this.getPrecio = function(producto,cantidad){         
        var precio = parseFloat(producto.precio_venta);
        if(this.datos.mayorista==1){
            var enc = false;
            //console.log(cantidad);
            for(var i in cantidades_mayoristas){
                if(!enc && parseInt(cantidad)>=parseInt(cantidades_mayoristas[i].desde)){
                    var campo = cantidades_mayoristas[i].campo;                                                    
                    precio = parseFloat(producto[campo]);
                    //enc = true;                                     
                }
            }
        }
        return precio;
    }

	this._addProduct = function(producto,cantidad){
        cantidad = cantidad!=undefined?cantidad:1;
        var pr = this.datos.productos;
        var enc = false;
        for(var i in pr){
            if(pr[i].producto.codigo==producto.codigo){
                enc = true;
                this.datos.productos[i].cantidad+=cantidad;				
            }
        }
        if(!enc){
                var precio = this.getPrecio(producto,cantidad);                                
                this.datos.productos.push({
                		producto:producto,                        
                        cantidad:cantidad,                        
                        precioventa:precio,
                        totalcondesc:precio,
                        total: 0
                });
        }

        this.updateData();
	}
        
    this.getBalanza = function(codigo,medida){
        if(codigo.toString().length!=13){
            return codigo;
        }
        medida = typeof medida == 'undefined'?'peso':medida;
        for(var i in codigo_balanza){
            var cc = codigo_balanza[i];
            var numerosBalanza = cc.toString().length;
            if(codigo.toString().substring(0,numerosBalanza)==cc){
                var peso = parseInt(codigo.toString().substring(7,12));
                peso = medida=='peso'?parseInt(peso)/1000:parseFloat(peso);
                codigo = parseInt(codigo.toString().substring(2,7));
                codigo = {
                    codigo: codigo,
                    cantidad: peso
                }
                return codigo;  
            }
        }
        return codigo;
    }

	this.addProduct = function(codigo){                                
		//buscamos el codigo
		var l = this;				
		if(codigo!=''){
	        codigo = codigo.split('*');
	        if(codigo.length==2){
	                cantidad = parseFloat(codigo[0]);
	                codigo = codigo[1];
	        }else{
	                codigo = codigo[0];
	                codigo = codigo.split('+');
	                if(codigo.length==2){
	                        cantidad = parseFloat(codigo[0]);
	                        codigo = codigo[1];	
	                }
	                else{
	                        cantidad = 1;
	                        codigo = codigo[0];	
	                }
	        }
	        
	        //Es balanza?                                        
	        codigo = this.getBalanza(codigo);                                        
	        cantidad = typeof codigo == 'object'?codigo.cantidad:cantidad;                                        
	        codigo = typeof codigo == 'object'?codigo.codigo:codigo;
	        //console.log(codigo); 
	        $.post(URI+'movimientos/productos/productos/json_list',{
	                'codigo':codigo,
	                'operator':'where',
	                'cliente':this.datos.cliente
	        },function(data){
	                data = JSON.parse(data);
	                if(data.length>0){
	                	if(vender_sin_stock==1 || (cantidad>0 && parseFloat(data[0].stock)>=cantidad) || (data[0].inventariable=='inactivo')){
	                        $("#codigoAdd").val('');
	                        l._addProduct(data[0],cantidad);
	                        $("#codigoAdd").attr('placeholder','Código de producto');
	                        $("#codigoAdd").removeClass('error');
	                    }else{
	                    	$("#codigoAdd").val('');
	                        $("#codigoAdd").attr('placeholder','Sin stock suficiente');
	                        $("#codigoAdd").addClass('error');
	                    }
	                }else{
	                        $("#codigoAdd").val('');
	                        $("#codigoAdd").attr('placeholder','Producto no encontrado');
	                        $("#codigoAdd").addClass('error');
	                }
	        });
		}
	}

	this.removeProduct = function(codigo){
		if(codigo!=''){
			var pr = this.datos.productos;
			var pr2 = [];
			for(var i in pr){
				if(pr[i].producto.codigo!=codigo){
					pr2.push(pr[i]);
				}
			}
		}
		this.datos.productos = pr2;
		this.updateData();
	}
}