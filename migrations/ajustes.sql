ALTER TABLE `ajustes` ADD IF NOT EXISTS `id_reporte_ticket_pagocliente` INT NULL AFTER `id_reporte_presupuestos`, ADD IF NOT EXISTS `id_reporte_pagocliente` INT NULL AFTER `id_reporte_ticket_pagocliente`;
#Permitir pago superior a saldo
ALTER TABLE `ajustes` ADD IF NOT EXISTS `permitir_pago_superior_a_saldo` BOOLEAN NULL DEFAULT FALSE AFTER `id_reporte_PedidosProveedores`;

ALTER TABLE ajustes ADD IF NOT EXISTS habilitar_cache INT(1) DEFAULT 0
ALTER TABLE compras ADD IF NOT EXISTS user_id INT(11) NOT NULL;
ALTER TABLE compras ADD IF NOT EXISTS fecha_registro DATETIME DEFAULT 0;

